import { createSlice } from '@reduxjs/toolkit';
import { getUsers } from './api';

const initialState = {
  userData: null,
  loading: false
};

const userSlice = createSlice({
  name: 'userSlice',
  initialState,
  extraReducers: (builder) => {
    builder.addCase(getUsers.pending, (state, action) => {
      state.loading = true;
    });
    builder.addCase(getUsers.fulfilled, (state, action) => {
      state.loading = false;
      state.userData = action.payload.data;
    });
    builder.addCase(getUsers.rejected, (state, action) => {
      state.loading = false;
    });
  }
});

const { reducer: userReducer } = userSlice;

export default userReducer;
