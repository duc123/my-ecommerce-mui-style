import { createAsyncThunk } from '@reduxjs/toolkit';
import axiosClient from '../../../lib/axiosClient';
import { addAlert } from '../alert';

export const getUsers = createAsyncThunk('getUsers', async (query, dispatch) => {
  try {
    const res = await axiosClient({
      url: `/admin/clients`,
      method: 'GET',
      params: query
    });
    return res;
  } catch (err) {
    dispatch.dispatch(
      addAlert({ severity: 'error', title: 'Error', content: err.response.data.error })
    );
  }
});
