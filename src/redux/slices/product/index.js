import { createSlice } from '@reduxjs/toolkit';
import { addProduct, deleteProduct, getProducts } from './api';

const initialState = {
  productData: null,
  loading: false,
  selectedProduct: null,
  searchQueries: {
    category: 'all',
    subCategory: 'all',
    productType: 'all',
    price: 'default'
  }
};

const productSlice = createSlice({
  name: 'productSlice',
  initialState,
  reducers: {
    selectProduct: (state, action) => {
      state.selectedProduct = action.payload;
    },
    unSelectProduct: (state) => {
      state.selectedProduct = null;
    },
    setQueries: (state, action) => {
      state.searchQueries = action.payload;
    },
    resetSearch: (state) => {
      state.searchQueries = {
        category: 'all',
        subCategory: 'all',
        productType: 'all',
        price: 'default'
      };
    }
  },
  extraReducers: (builder) => {
    builder.addCase(getProducts.pending, (state) => {
      state.loading = true;
    });
    builder.addCase(getProducts.fulfilled, (state, action) => {
      state.loading = false;
      const { data } = action.payload;
      state.productData = data;
    });
    builder.addCase(getProducts.rejected, (state) => {
      state.loading = false;
    });
    builder.addCase(addProduct.pending, (state) => {
      state.loading = true;
    });
    builder.addCase(addProduct.fulfilled, (state, action) => {
      state.loading = false;
    });
    builder.addCase(addProduct.rejected, (state) => {
      state.loading = false;
    });
    // DELETE PRODUCT
    builder.addCase(deleteProduct.pending, (state) => {
      state.loading = true;
    });
    builder.addCase(deleteProduct.fulfilled, (state, action) => {
      state.loading = false;
      const newData = state.productData;
      newData.products = newData.products.filter((el) => el._id !== action.payload);
      state.productData = newData;
    });
    builder.addCase(deleteProduct.rejected, (state) => {
      state.loading = false;
    });
  }
});

const { reducer: productReducer } = productSlice;

export const {
  actions: { selectProduct, unSelectProduct, setQueries, resetSearch }
} = productSlice;

export default productReducer;
