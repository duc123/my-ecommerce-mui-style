import { createAsyncThunk } from '@reduxjs/toolkit';
import axiosClient from '../../../lib/axiosClient';
import { addAlert } from '../alert';

export const addProduct = createAsyncThunk('addProduct', async (data, dispatchApi) => {
  try {
    const res = await axiosClient({
      method: 'POST',
      url: '/admin/products',
      data
    });
    dispatchApi.dispatch(
      addAlert({ severity: 'success', title: 'Success', content: 'New product is added' })
    );
    return res;
  } catch (err) {
    throw err.response.data.error;
  }
});

export const getProducts = createAsyncThunk('getProducts', async (params, dispatchApi) => {
  try {
    const res = await axiosClient({
      method: 'GET',
      url: '/admin/products/search',
      params
    });
    return res;
  } catch (err) {
    dispatchApi.dispatch(
      addAlert({ severity: 'error', title: 'Error', content: err.response.data.error })
    );
  }
});

export const deleteProduct = createAsyncThunk('deleteProduct', async (id, dispatchApi) => {
  try {
    await axiosClient({
      method: 'DELETE',
      url: `/admin/products/delete/${id}`
    });
    dispatchApi.dispatch(
      addAlert({ severity: 'success', title: 'Success', content: 'Product is deleted' })
    );
    return id;
  } catch (err) {
    throw err.response.data.error;
  }
});
