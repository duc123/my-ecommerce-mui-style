import { createSlice } from '@reduxjs/toolkit';
import { loginRequest, logoutRequest } from './api';

const auth = localStorage.getItem('auth');

const initialState = {
  auth: auth ? JSON.parse(auth) : '',
  isLoading: false,
  error: ''
};

const saveJwt = (token) => {
  localStorage.setItem('auth', JSON.stringify(token));
};

const removeJwt = () => localStorage.removeItem('auth');

const loginSlice = createSlice({
  name: 'loginSlice',
  initialState,
  reducers: {
    removeLoginError: (state) => {
      state.error = '';
    },
    forceLogout: (state) => {
      state.isLoading = false;
      removeJwt();
      state.auth = '';
    }
  },
  extraReducers: (builder) => {
    // LOGIN
    builder.addCase(loginRequest.pending, (state) => {
      state.error = '';
      state.isLoading = true;
    });
    builder.addCase(loginRequest.fulfilled, (state, action) => {
      state.error = '';
      state.isLoading = false;
      const {
        data: { token }
      } = action.payload;
      state.auth = token;
      saveJwt(token);
    });
    builder.addCase(loginRequest.rejected, (state, action) => {
      state.isLoading = false;
      state.error = action.error.message;
    });
    // LOGOUT
    builder.addCase(logoutRequest.pending, (state) => {
      state.error = '';
      state.isLoading = true;
    });
    builder.addCase(logoutRequest.fulfilled, (state, action) => {
      state.auth = '';
      removeJwt();
      state.isLoading = false;
    });
    builder.addCase(logoutRequest.rejected, (state, action) => {
      state.isLoading = false;
    });
  }
});

const {
  reducer: loginReducer,
  actions: { removeLoginError, forceLogout }
} = loginSlice;

export { removeLoginError, forceLogout };

export default loginReducer;
