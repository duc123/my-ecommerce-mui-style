import { createAsyncThunk } from '@reduxjs/toolkit';
import { forceLogout } from '.';
import axiosClient from '../../../lib/axiosClient';
import { addAlert, removeAllAlert } from '../alert';

export const loginRequest = createAsyncThunk('loginRequest', async (data) => {
  try {
    const res = await axiosClient({
      method: 'POST',
      url: 'auth/admin/login',
      data
    });
    return res;
  } catch (err) {
    throw err.response.data.error;
  }
});

export const logoutRequest = createAsyncThunk('logoutRequest', async (data, dispatch) => {
  try {
    const res = await axiosClient({
      method: 'POST',
      url: 'auth/admin/logout'
    });
    return res;
  } catch (err) {
    dispatch.dispatch(
      addAlert({ severity: 'error', title: 'Error', content: err.response.data.error })
    );
    if (err.response.data.error === 'Unauthenticated') {
      dispatch.dispatch(forceLogout());
      dispatch.dispatch(removeAllAlert());
    }
  }
});
