import { createSlice, current } from '@reduxjs/toolkit';
import { v4 as uuid } from 'uuid';

const initialState = {
  alerts: [],
  list: []
};

const alertSlice = createSlice({
  name: 'alertSlice',
  initialState,
  reducers: {
    addAlert: (state, action) => {
      const alert = { ...action.payload, id: uuid() };
      const newList = [...current(state).alerts, alert];
      state.alerts = newList;
    },
    removeAlert: (state, action) => {
      state.alerts = state.alerts.filter((el) => el.id !== action.payload);
    },
    removeAllAlert: (state) => {
      state.alerts = [];
    }
  }
});

const { reducer: alertReducer } = alertSlice;

export const {
  actions: { addAlert, removeAlert, removeAllAlert }
} = alertSlice;

export default alertReducer;
