import { createSlice } from '@reduxjs/toolkit';

const initialState = {
  open: false,
  title: '',
  content: '',
  callback: null,
  loading: false
};

const modalSlice = createSlice({
  name: 'modalSlice',
  initialState,
  reducers: {
    showModal: (state, action) => {
      const { title, content, callback } = action.payload;
      state.open = true;
      state.title = title;
      state.content = content;
      state.callback = callback;
    },
    hideModal: (state) => {
      state.open = false;
      state.callback = null;
    }
  }
});

const { reducer: modalReducer } = modalSlice;

export const {
  actions: { showModal, hideModal }
} = modalSlice;

export default modalReducer;
