import { createSlice } from '@reduxjs/toolkit';
import { getOrders } from './api';

const initialState = {
  orderData: null,
  loading: false
};

const orderSlice = createSlice({
  name: 'orderSlice',
  initialState,
  extraReducers: (builder) => {
    builder.addCase(getOrders.pending, (state, action) => {
      state.loading = true;
    });
    builder.addCase(getOrders.fulfilled, (state, action) => {
      state.loading = false;
      state.orderData = action.payload.data;
    });
    builder.addCase(getOrders.rejected, (state, action) => {
      state.loading = false;
    });
  }
});

const { reducer: orderReducer } = orderSlice;

export default orderReducer;
