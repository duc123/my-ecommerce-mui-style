import { createSlice } from '@reduxjs/toolkit';
import { addSubCategory, createCategory, getCategoryList } from './api.js';

const initialState = {
  categories: [],
  loading: false,
  error: ''
};

const categorySlice = createSlice({
  name: 'categorySlice',
  initialState,
  extraReducers: (builder) => {
    //   GET category list
    builder.addCase(getCategoryList.pending, (state) => {
      state.error = '';
      state.loading = true;
    });
    builder.addCase(getCategoryList.fulfilled, (state, action) => {
      state.error = '';
      state.loading = false;
      const {
        data: { categories }
      } = action.payload;
      state.categories = categories;
    });
    builder.addCase(getCategoryList.rejected, (state, action) => {
      state.error = action.error.message;
      state.loading = false;
    });

    // CreateCategory
    builder.addCase(createCategory.pending, (state) => {
      state.error = '';
      state.loading = true;
    });
    builder.addCase(createCategory.fulfilled, (state, action) => {
      state.error = '';
      state.loading = false;
      const {
        data: { category }
      } = action.payload;
      state.categories = [...state.categories, category];
    });
    builder.addCase(createCategory.rejected, (state, action) => {
      state.error = action.error.message;
      state.loading = false;
    });

    // Add subcategory
    builder.addCase(addSubCategory.pending, (state) => {
      state.error = '';
      state.loading = true;
    });
    builder.addCase(addSubCategory.fulfilled, (state, action) => {
      state.error = '';
      state.loading = false;
      const {
        data: { category }
      } = action.payload;
      const updatedCategories = [...state.categories];
      const index = updatedCategories.findIndex((el) => el._id === category._id);
      updatedCategories[index] = category;
      state.categories = updatedCategories;
    });
    builder.addCase(addSubCategory.rejected, (state, action) => {
      state.error = action.error.message;
      state.loading = false;
    });
  }
});

const { reducer: categoryReducer } = categorySlice;

export default categoryReducer;
