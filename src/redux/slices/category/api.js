import { createAsyncThunk } from '@reduxjs/toolkit';
import axiosClient from '../../../lib/axiosClient';

export const getCategoryList = createAsyncThunk('getCategory', async () => {
  try {
    const res = await axiosClient({
      method: 'GET',
      url: '/categories'
    });
    return res;
  } catch (err) {
    throw err.response.data.error;
  }
});

export const createCategory = createAsyncThunk('createCategory', async (data) => {
  try {
    const res = await axiosClient({
      method: 'POST',
      url: '/admin/categories',
      data
    });
    return res;
  } catch (err) {
    throw err.response.data.error;
  }
});

export const addSubCategory = createAsyncThunk('addSubCategory', async (data) => {
  const { sub, id } = data;
  try {
    const res = await axiosClient({
      method: 'PUT',
      url: `/admin/categories/add-sub/${id}`,
      data: { sub }
    });
    return res;
  } catch (err) {
    throw err.response.data.error;
  }
});
