import { createSlice } from '@reduxjs/toolkit';
import {
  getBuyers,
  getCryptoData,
  getNumberOfClients,
  getNumberOfProducts,
  getRevenue
} from './api';

const initialState = {
  numProducts: 0,
  numClients: 0,
  revenue: 0,
  numBuyers: 0,
  crypto: null
};

const statisticSlice = createSlice({
  name: 'statisticSlice',
  initialState,
  extraReducers: (builder) => {
    // NumProducts
    builder.addCase(getNumberOfProducts.pending, (state, action) => {});
    builder.addCase(getNumberOfProducts.fulfilled, (state, action) => {
      state.numProducts = action.payload.data.count;
    });
    builder.addCase(getNumberOfProducts.rejected, (state, action) => {});

    // NumClients
    builder.addCase(getNumberOfClients.pending, (state, action) => {});
    builder.addCase(getNumberOfClients.fulfilled, (state, action) => {
      state.numClients = action.payload.data.count;
    });
    builder.addCase(getNumberOfClients.rejected, (state, action) => {});

    // Revenue
    builder.addCase(getRevenue.pending, (state, action) => {});
    builder.addCase(getRevenue.fulfilled, (state, action) => {
      state.revenue = action.payload.data.profits;
    });
    builder.addCase(getRevenue.rejected, (state, action) => {});

    // NumBuyers
    builder.addCase(getBuyers.pending, (state, action) => {});
    builder.addCase(getBuyers.fulfilled, (state, action) => {
      state.numBuyers = action.payload.data.count;
    });
    builder.addCase(getBuyers.rejected, (state, action) => {});

    // Crpyto
    builder.addCase(getCryptoData.pending, (state, action) => {});
    builder.addCase(getCryptoData.fulfilled, (state, action) => {
      state.crypto = action.payload;
    });
    builder.addCase(getCryptoData.rejected, (state, action) => {});
  }
});

const { reducer: statisticReducer } = statisticSlice;
export default statisticReducer;
