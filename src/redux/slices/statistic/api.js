import { createAsyncThunk } from '@reduxjs/toolkit';
import axiosClient from '../../../lib/axiosClient';
import { addAlert } from '../alert';

export const getNumberOfProducts = createAsyncThunk(
  'getNumberOfProducts',
  async (data, dispatch) => {
    try {
      const res = await axiosClient({
        method: 'GET',
        url: `/admin/products/count`
      });
      return res;
    } catch (err) {
      dispatch.dispatch(
        addAlert({ severity: 'error', title: 'Error', content: err.response.data.error })
      );
      throw err.response.data.error;
    }
  }
);

export const getNumberOfClients = createAsyncThunk('getNumberOfClients', async (data, dispatch) => {
  try {
    const res = await axiosClient({
      method: 'GET',
      url: `/admin/clients/count`
    });
    return res;
  } catch (err) {
    dispatch.dispatch(
      addAlert({ severity: 'error', title: 'Error', content: err.response.data.error })
    );

    throw err.response.data.error;
  }
});

export const getRevenue = createAsyncThunk('getRevenue', async (data, dispatch) => {
  try {
    const res = await axiosClient({
      method: 'GET',
      url: `/admin/orders/sales`
    });
    return res;
  } catch (err) {
    dispatch.dispatch(
      addAlert({ severity: 'error', title: 'Error', content: err.response.data.error })
    );

    throw err.response.data.error;
  }
});

export const getBuyers = createAsyncThunk('getBuyers', async (data, dispatch) => {
  try {
    const res = await axiosClient({
      method: 'GET',
      url: `/admin/orders/getBuyers`
    });
    return res;
  } catch (err) {
    dispatch.dispatch(
      addAlert({ severity: 'error', title: 'Error', content: err.response.data.error })
    );

    throw err.response.data.error;
  }
});

export const getCryptoData = createAsyncThunk('getCrpyroData', async (name) => {
  try {
    const res = await axiosClient({
      baseURL: `https://www.alphavantage.co/query?function=DIGITAL_CURRENCY_DAILY&symbol=${name}&market=CNY&apikey=${process.env.REACT_APP_ALPHA_KEY}`,
      headers: { 'User-Agent': 'request' },
      method: 'GET'
    });
    return res;
  } catch (err) {
    console.log(err);
  }
});
