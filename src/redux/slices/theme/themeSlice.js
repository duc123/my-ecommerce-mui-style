import { createSlice } from '@reduxjs/toolkit';

const initialState = {
  isLight: false
};

const themeSlice = createSlice({
  name: 'themeSlice',
  initialState,
  reducers: {
    swithTheme: (state) => {
      state.isLight = !state.isLight;
    }
  }
});

const { reducer: themeReducer } = themeSlice;

export const {
  actions: { swithTheme }
} = themeSlice;

export default themeReducer;
