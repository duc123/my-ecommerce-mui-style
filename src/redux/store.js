import { configureStore } from '@reduxjs/toolkit';
import loginReducer from './slices/login';
import themeReducer from './slices/theme/themeSlice';
import categoryReducer from './slices/category';
import productReducer from './slices/product';
import alertReducer from './slices/alert/index';
import modalReducer from './slices/modal';
import userReducer from './slices/user';
import orderReducer from './slices/order';
import statisticReducer from './slices/statistic/slice';

export const store = configureStore({
  reducer: {
    loginReducer,
    themeReducer,
    categoryReducer,
    productReducer,
    modalReducer,
    alertReducer,
    userReducer,
    orderReducer,
    statisticReducer
  }
});
