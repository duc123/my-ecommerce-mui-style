import axiosClient from '../lib/axiosClient';

export const editVariantInfo = async (data, productId, varId) => {
  try {
    const res = await axiosClient({
      method: 'PUT',
      data,
      url: `/admin/products/editVariant/${productId}?varId=${varId}`
    });

    return res.data.product;
  } catch (err) {
    console.log(err);
    throw err.response.data.error;
  }
};
