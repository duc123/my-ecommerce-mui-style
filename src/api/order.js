import axiosClient from '../lib/axiosClient';

export const getSingleOrder = async (id) => {
  try {
    const res = await axiosClient({
      url: `/admin/orders/getSingle/${id}`
    });
    return res.data.order;
  } catch (err) {
    throw err.reponse.data.error;
  }
};

export const markPaid = async (id) => {
  try {
    const res = await axiosClient({
      url: `/admin/orders/payment/done?id=${id}`,
      method: 'PUT'
    });
    return res.data.order;
  } catch (err) {
    throw err.reponse.data.error;
  }
};

export const markUnPaid = async (id) => {
  try {
    const res = await axiosClient({
      url: `/admin/orders/payment/undone?id=${id}`,
      method: 'PUT'
    });
    return res.data.order;
  } catch (err) {
    throw err.reponse.data.error;
  }
};

export const markDelivered = async (id) => {
  try {
    const res = await axiosClient({
      url: `/admin/orders/delivery/done?id=${id}`,
      method: 'PUT'
    });
    return res.data.order;
  } catch (err) {
    throw err.reponse.data.error;
  }
};

export const markUnDelivered = async (id) => {
  try {
    const res = await axiosClient({
      url: `/admin/orders/delivery/undone?id=${id}`,
      method: 'PUT'
    });
    return res.data.order;
  } catch (err) {
    throw err.reponse.data.error;
  }
};
