import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { useSelector } from 'react-redux';
import style from './style.module.scss';
import { IconButton, Tooltip } from '@mui/material';
import { AddOutlined } from '@mui/icons-material';
import AddSubCategory from '../AddSubCategory';

const CategoryItem = ({ item }) => {
  const { isLight } = useSelector((state) => state.themeReducer);

  const [open, setOpen] = useState(false);

  const openModal = () => setOpen(true);

  const closeModal = () => setOpen(false);

  const { title, subs, _id } = item;

  const renderSubCategories = () => {
    return (
      <>
        {subs.map((sub) => (
          <span className={style.sub} key={sub}>
            {sub}
          </span>
        ))}
      </>
    );
  };

  return (
    <div className={`${isLight ? `componentBg` : `componentBgDark ${style.dark}`}`}>
      <AddSubCategory open={open} closeModal={closeModal} category={item} />
      <h3 className={style.title}>{title}</h3>

      <div className={style.subs}>
        {subs?.length > 0 && renderSubCategories()}
        <Tooltip title="Add subcategory">
          <IconButton onClick={openModal} className={style.iconBtn} size="small">
            <AddOutlined />
          </IconButton>
        </Tooltip>
      </div>
    </div>
  );
};

CategoryItem.propTypes = {
  item: PropTypes.object
};

export default CategoryItem;
