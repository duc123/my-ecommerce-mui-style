import React, { useEffect } from 'react';
import PropTypes from 'prop-types';
import { Alert, AlertTitle } from '@mui/material';
import style from './style.module.scss';
import { useDispatch } from 'react-redux';
import { removeAlert } from '../../redux/slices/alert';
import { logoutRequest } from '../../redux/slices/login/api';

const CustomizeAlert = ({ alert }) => {
  const { title, content, severity, id } = alert;
  const dispatch = useDispatch();

  const handleClose = () => dispatch(removeAlert(id));

  useEffect(() => {
    if (content === 'Unauthenticated') dispatch(logoutRequest());
  }, [content]);

  return (
    <Alert onClose={handleClose} variant="filled" className={style.alert} severity={severity}>
      {title && <AlertTitle>{title}</AlertTitle>}
      {content}
    </Alert>
  );
};

CustomizeAlert.propTypes = {
  alert: PropTypes.object
};

export default CustomizeAlert;
