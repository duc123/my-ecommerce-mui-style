import { CloseOutlined, EditOutlined } from '@mui/icons-material';
import { IconButton } from '@mui/material';
import React, { useEffect, useRef, useState } from 'react';
import CustomizeInput from '../CustomizeInput';
import style from './style.module.scss';
import PropTypes from 'prop-types';
import { editVariantInfo } from '../../api/product';
import Loading from '../Loading';

const StockElement = ({ stock, onChange, onEdit }) => {
  const handleSubmit = (e) => {
    e.preventDefault();
    onEdit();
  };

  return (
    <form onSubmit={handleSubmit}>
      <CustomizeInput type="number" value={stock} onChange={onChange} size="small" />
    </form>
  );
};

StockElement.propTypes = {
  stock: PropTypes.number,
  onChange: PropTypes.func,
  onEdit: PropTypes.func
};

const Stock = ({ stock }) => {
  return <span>{stock}</span>;
};

Stock.propTypes = {
  stock: PropTypes.number
};

const StockCellElement = ({ product, setProduct, variant }) => {
  const [stock, setStock] = useState(null);

  const [loading, setLoading] = useState(false);

  const mainEl = useRef(null);

  const [inputVisible, setInputVisible] = useState(false);

  const showInput = () => setInputVisible(true);
  const hideInput = () => setInputVisible(false);

  const onChange = (e) => {
    setStock(e.target.value);
  };

  const onEdit = async () => {
    setLoading(true);
    try {
      const newProduct = await editVariantInfo({ stock: +stock }, product._id, variant.id);
      setProduct(newProduct);
      setInputVisible(false);
    } catch (err) {
      console.log(err);
    }
    setLoading(false);
  };

  useEffect(() => {
    if (variant) setStock(variant.stock);
  }, [variant]);

  return (
    <div className={style.container} ref={mainEl}>
      {loading && <Loading loading={loading} />}
      <div className={style.main}>
        {inputVisible ? (
          <StockElement onEdit={onEdit} stock={stock} onChange={onChange} />
        ) : (
          <Stock stock={stock} />
        )}
      </div>

      <p className={style.iconContainer}>
        {!inputVisible ? (
          <IconButton className={style.icon} onClick={showInput} size="small">
            <EditOutlined />
          </IconButton>
        ) : (
          <IconButton className={style.icon} onClick={hideInput} size="small">
            <CloseOutlined />
          </IconButton>
        )}
      </p>
    </div>
  );
};

StockCellElement.propTypes = {
  product: PropTypes.object,
  setProduct: PropTypes.func,
  variant: PropTypes.object
};

export default StockCellElement;
