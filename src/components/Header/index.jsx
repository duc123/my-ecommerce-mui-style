import React from 'react';
import PropTypes from 'prop-types';
import {
  DarkModeOutlined,
  LightModeOutlined,
  LogoutOutlined,
  MenuOutlined
} from '@mui/icons-material';
import style from './style.module.scss';
import { IconButton, Tooltip } from '@mui/material';
import { useDispatch, useSelector } from 'react-redux';
import { swithTheme } from '../../redux/slices/theme/themeSlice';
import CustomizeButton from '../../components/CustomizeButton';
import { logoutRequest } from '../../redux/slices/login/api';
import Loading from '../Loading';

const Header = ({ isExpand, toogleExpand }) => {
  const dispatch = useDispatch();

  const { isLight } = useSelector((state) => state.themeReducer);

  const handleChangeTheme = () => dispatch(swithTheme());

  const { isLoading } = useSelector((state) => state.loginReducer);

  return (
    <div className={`${style.header} ${!isLight && style.dark} ${isExpand && style.recoil}`}>
      <IconButton onClick={toogleExpand}>
        <MenuOutlined />
      </IconButton>

      <Loading loading={isLoading} />

      <div>
        <CustomizeButton
          onClick={() => dispatch(logoutRequest())}
          variant="contained"
          title="Logout"
          endIcon={<LogoutOutlined />}
          size="xs"
          color="neutral"
        />
        <Tooltip onClick={handleChangeTheme} title={isLight ? 'light theme' : 'dark theme'}>
          <IconButton sx={{ marginLeft: '10px' }}>
            {isLight ? <LightModeOutlined /> : <DarkModeOutlined />}
          </IconButton>
        </Tooltip>
      </div>
    </div>
  );
};

Header.propTypes = {
  isExpand: PropTypes.bool,
  toogleExpand: PropTypes.func
};

export default Header;
