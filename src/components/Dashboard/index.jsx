import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { Outlet, useNavigate } from 'react-router-dom';
import { removeAllAlert } from '../../redux/slices/alert/index.js';

import { getCategoryList } from '../../redux/slices/category/api.js';
import {
  getBuyers,
  getCryptoData,
  getNumberOfClients,
  getNumberOfProducts,
  getRevenue
} from '../../redux/slices/statistic/api.js';
import Alerts from '../Alerts';
import ConfirmModal from '../ConfirmModal';
import Header from '../Header';
import Loading from '../Loading/index.jsx';
import Sidebar from '../SideBar';
import style from './style.module.scss';

const Dashboard = () => {
  const { auth } = useSelector((state) => state.loginReducer);
  const { isLight } = useSelector((state) => state.themeReducer);
  const navigate = useNavigate();
  const dispatch = useDispatch();

  const [loading, setLoading] = useState(false);

  useEffect(() => {
    if (!auth) {
      navigate('/login');
    } else {
      setLoading(true);
      Promise.all([
        dispatch(getCategoryList()),
        dispatch(getCryptoData('BTC')),
        dispatch(getNumberOfProducts()),
        dispatch(getNumberOfClients()),
        dispatch(getRevenue()),
        dispatch(getBuyers())
      ])
        .then((values) => {})
        .catch((err) => {
          console.log(err);
        });
      setLoading(false);
    }
  }, [auth]);

  const [isExpand, setIsExpand] = useState(true);

  const toogleExpand = () => setIsExpand(!isExpand);

  return (
    <div className={`${style.dashboard} ${!isLight && style.dark} ${isExpand && style.recoil}`}>
      <Loading loading={loading} />
      <Alerts />
      <ConfirmModal />
      <Sidebar isExpand={isExpand} setIsExpand={setIsExpand} />
      <div className={style.container}>
        <Header isExpand={isExpand} toogleExpand={toogleExpand} />

        <Outlet />
      </div>
    </div>
  );
};

export default Dashboard;
