import { FormControl, FormControlLabel, FormLabel, Radio, RadioGroup } from '@mui/material';
import React, { useState } from 'react';
import { useNavigate } from 'react-router-dom';
import CustomizeButton from '../CustomizeButton';
import style from './style.module.scss';

const SelectProductType = () => {
  const [type, setType] = useState('');

  const handleChange = (e) => {
    setType(e.target.value);
  };

  const navigate = useNavigate();

  const handleClick = () => {
    navigate(`/addProduct/add?type=${type}`);
  };

  return (
    <div className={style.container}>
      <h2>Select product type</h2>
      <div className={style.selectContainer}>
        <FormControl>
          <RadioGroup
            aria-labelledby="demo-controlled-radio-buttons-group"
            name="controlled-radio-buttons-group"
            value={type}
            onChange={handleChange}>
            <FormControlLabel
              className={style.block}
              value="simple"
              control={<Radio />}
              label="Create a simple product"
            />
            <FormControlLabel
              value="complex"
              control={<Radio />}
              label="Create a complex product"
            />
          </RadioGroup>
        </FormControl>
      </div>
      <CustomizeButton
        onClick={handleClick}
        disabled={type ? false : true}
        title="Next"
        color="primary"
        variant="contained"
      />
    </div>
  );
};

export default SelectProductType;
