import { CloseOutlined, UploadOutlined } from '@mui/icons-material';
import { Grid, IconButton, Modal } from '@mui/material';
import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import style from './style.module.scss';
import { yupResolver } from '@hookform/resolvers/yup';
import * as yup from 'yup';
import { Controller, useForm } from 'react-hook-form';
import { createYupSchema } from '../../utils/createYupSchema';
import CustomizeInput from '../CustomizeInput';
import CustomizeButton from '../CustomizeButton';
import { fileListToBase64 } from '../../utils/toBase64';
import { addProductVariant } from '../../redux/slices/product/productActions';
import { closeModal } from '../../redux/slices/product';

const AddProductVariant = () => {
  const { modalOpen, selectedVariant, productOfVariant, loading } = useSelector(
    (state) => state.productReducer
  );

  const [displayedImages, setDisplayedImages] = useState([]);

  const { attributes } = productOfVariant;

  const { isLight } = useSelector((state) => state.themeReducer);

  const initialData = [
    {
      id: 'price',
      label: 'Price',
      type: 'number',
      validationType: 'number',
      validations: [
        {
          type: 'required',
          params: ['This filed is required']
        },
        {
          type: 'moreThan',
          params: [0, 'Price must be greater than 0']
        }
      ]
    },
    {
      id: 'stock',
      label: 'Stock',
      type: 'number',
      validationType: 'number',
      validations: [
        {
          type: 'required',
          params: ['This filed is required']
        },
        {
          type: 'min',
          params: [0, 'Stock can not be nagative']
        }
      ]
    }
  ];

  attributes.forEach((el) => {
    const schemaItem = {
      id: el,
      label: el,
      type: 'string',
      validationType: 'string',
      validations: [{ type: 'required', params: ['This field is required'] }]
    };
    initialData.push(schemaItem);
  });

  const yupSchema = initialData.reduce(createYupSchema, {});
  const schema = yup.object().shape({
    ...yupSchema,
    images: yup.mixed().test('images', 'Images are required', (values) => {
      return values?.length > 0;
    })
  });

  const { control, handleSubmit, watch } = useForm({
    resolver: yupResolver(schema),
    mode: 'all',
    defaultValues: {
      price: ''
    }
  });

  const watchImages = watch('images');
  console.log(watchImages);
  const dispatch = useDispatch();

  const handleClose = () => {
    dispatch(closeModal());
  };

  useEffect(() => {
    if (watchImages?.length > 0) {
      fileListToBase64(watchImages).then((values) => {
        setDisplayedImages(values);
      });
    } else {
      setDisplayedImages([]);
    }
  }, [watchImages]);

  const formItem = () => {
    return (
      <>
        {initialData.map((el) => (
          <Grid key={el.id} item xs={12} md={6}>
            <Controller
              render={({ fieldState: { error }, field: { value, onChange } }) => (
                <CustomizeInput
                  value={value}
                  onChange={onChange}
                  label={el.label}
                  error={error}
                  type={el.type}
                />
              )}
              name={el.id}
              control={control}
            />
          </Grid>
        ))}
      </>
    );
  };

  const submit = async (data) => {
    const formData = new FormData();

    const keys = Object.keys(data);

    for (const key of keys) {
      if (key !== 'images') {
        formData.append(key, data[key]);
      }
    }

    for (const file of data.images) {
      formData.append('images', file);
    }
    formData.append('product_id', productOfVariant._id);

    const res = await dispatch(addProductVariant(formData));
    if (res.payload) dispatch(closeModal());
  };

  return (
    <Modal open={modalOpen} onClose={handleClose}>
      <form
        onSubmit={handleSubmit(submit)}
        className={`${style.content} ${!isLight && style.dark}`}>
        <div className={style.header}>
          <h3>{selectedVariant ? 'Edit variant' : 'Add variant'}</h3>
          <IconButton onClick={handleClose}>
            <CloseOutlined />
          </IconButton>
        </div>
        <div className={style.body}>
          <Grid container spacing={2}>
            {formItem()}
            <Grid item xs={12}>
              <label htmlFor="contained-button-file">
                <Controller
                  defaultValue=""
                  render={({ field: { onChange, value }, fieldState: { error } }) => {
                    return (
                      <>
                        <input
                          style={{ display: 'none' }}
                          accept="image/*"
                          type="file"
                          value={value.files}
                          onChange={(e) => {
                            onChange(e.target.files);
                          }}
                          placeholder="Images"
                          id="contained-button-file"
                          multiple
                        />
                        <div className={`${style.upload} ${!isLight && style.dark}`}>
                          <p>Upload images</p>
                          <UploadOutlined />
                        </div>
                        <small className={style.error}>{error && error.message}</small>
                      </>
                    );
                  }}
                  name="images"
                  control={control}
                />
              </label>
              {displayedImages.length > 0 ? (
                <div className={style.images}>
                  {displayedImages.map((img, i) => (
                    <img key={i} src={img} />
                  ))}
                </div>
              ) : (
                ''
              )}
            </Grid>
          </Grid>
        </div>

        <div className={style.footer}>
          <CustomizeButton
            type="button"
            variant="contained"
            onClick={handleClose}
            title="Cancel"
            size="large"
            color="neutral"
          />
          <CustomizeButton
            type="submit"
            variant="contained"
            title="Save"
            size="large"
            color="primary"
            loading={loading}
            disabled={loading}
          />
        </div>
      </form>
    </Modal>
  );
};

export default AddProductVariant;
