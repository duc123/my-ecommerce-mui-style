import { CloseOutlined } from '@mui/icons-material';
import { IconButton, Modal } from '@mui/material';
import React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { hideModal } from '../../redux/slices/modal';
import CustomizeButton from '../CustomizeButton';
import style from './style.module.scss';

const ConfirmModal = () => {
  const { open, title, content, callback } = useSelector((state) => state.modalReducer);
  const { isLight } = useSelector((state) => state.themeReducer);

  const dispatch = useDispatch();

  const handleClose = () => dispatch(hideModal());

  const onOk = async () => {
    await callback();
    handleClose();
  };

  return (
    <Modal onClose={handleClose} open={open}>
      <div className={`${style.content} ${!isLight && style.dark}`}>
        <div className={style.header}>
          <div></div>
          <IconButton onClick={handleClose}>
            <CloseOutlined />
          </IconButton>
        </div>
        <div className={style.body}>
          <h3>{title}</h3>
          <p>{content}</p>
        </div>
        <div className={style.footer}>
          <CustomizeButton
            title="Cancel"
            size="large"
            type="button"
            color="neutral"
            variant="contained"
            onClick={handleClose}
          />
          <CustomizeButton
            title="Ok"
            size="large"
            type="butotn"
            color="error"
            variant="contained"
            onClick={onOk}
          />
        </div>
      </div>
    </Modal>
  );
};

export default ConfirmModal;
