import { Pagination } from '@mui/material';
import React from 'react';
import Proptypes from 'prop-types';

const CustomizePagination = ({ count, defaultPage, disabled, onChange, page }) => {
  return (
    <Pagination
      count={count}
      disabled={disabled}
      onChange={onChange}
      page={page}
      defaultPage={defaultPage}
      color="primary"
    />
  );
};

CustomizePagination.propTypes = {
  count: Proptypes.number,
  defaultPage: Proptypes.number,
  disabled: Proptypes.bool,
  onChange: Proptypes.func,
  page: Proptypes.number
};

export default CustomizePagination;
