import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import CustomizeButton from '../CustomizeButton';
import style from './style.module.scss';
import Proptypes from 'prop-types';
import { Controller, useForm } from 'react-hook-form';
import { yupResolver } from '@hookform/resolvers/yup';
import * as yup from 'yup';
import { UploadOutlined } from '@mui/icons-material';
import { fileListToBase64 } from '../../utils/toBase64';
import { addAlert } from '../../redux/slices/alert';
import axiosClient from '../../lib/axiosClient';
import Loading from '../Loading';

const SingleVariantImage = ({ val, images, id, setProduct }) => {
  const [displayImages, setDisplayImages] = useState([]);
  const { isLight } = useSelector((state) => state.themeReducer);

  const [loading, setLoading] = useState(false);

  const dispatch = useDispatch();

  useEffect(() => {
    setDisplayImages(images);
  }, [images]);

  const schema = yup.object().shape({
    images: yup.mixed().test('images', 'Images are required', (values) => {
      return values?.length > 0;
    })
  });

  const {
    watch,
    control,
    handleSubmit,
    formState: { errors }
  } = useForm({
    resolver: yupResolver(schema)
  });

  const watchedImages = watch('images');

  const editVariantImages = async (data) => {
    setLoading(true);
    try {
      const res = await axiosClient({
        method: 'PUT',
        data: data,
        url: `/admin/products/addVariantImg/${id}?attrValue=${val}`
      });
      const { updatedProduct } = res.data;
      setProduct(updatedProduct);
    } catch (err) {
      dispatch(addAlert({ severity: 'error', title: 'Error', content: err.response.data.error }));
    }
    setLoading(false);
  };

  const submit = (data) => {
    const formData = new FormData();
    for (let file of data.images) {
      formData.append('images', file);
    }
    editVariantImages(formData);
  };

  useEffect(() => {
    if (watchedImages?.length > 0) {
      fileListToBase64(watchedImages).then((values) => {
        setDisplayImages(values);
      });
    } else {
      if (images.length <= 0) setDisplayImages([]);
    }
  }, [watchedImages]);

  return (
    <form onSubmit={handleSubmit(submit)} className={style.variant}>
      <div className={style.varHead}>
        <p className={style.key}>{val}</p>

        <Loading loading={loading} />

        {images?.length <= 0 && (
          <label htmlFor="contained-button-file">
            <Controller
              defaultValue=""
              render={({ field: { onChange, value }, fieldState: { error } }) => {
                return (
                  <>
                    <input
                      style={{ display: 'none' }}
                      accept="image/*"
                      type="file"
                      value={value.files}
                      onChange={(e) => {
                        onChange(e.target.files);
                      }}
                      placeholder="Images"
                      id="contained-button-file"
                      multiple
                    />

                    <div className={style.uploadBlock}>
                      <CustomizeButton title="Add images" component="span" />
                    </div>
                  </>
                );
              }}
              name="images"
              control={control}
            />
          </label>
        )}
      </div>
      <div className={style.imagesContainer}>
        {displayImages.length > 0 ? (
          <div className={style.imgBlock}>
            {displayImages.map((img, i) => (
              <img key={i} src={img} />
            ))}
          </div>
        ) : (
          <div className={style.no}>
            <div className={style.noBlock}>
              <p>No images</p>
            </div>
            {errors.images && <small className={style.error}>{errors.images.message}</small>}
          </div>
        )}
      </div>

      {images?.length <= 0 && (
        <CustomizeButton title="Save" variant="contained" color="secondary" type="submit" />
      )}
    </form>
  );
};

SingleVariantImage.propTypes = {
  val: Proptypes.string,
  images: Proptypes.array
};

const VariantImages = ({ product, setProduct }) => {
  const { isLight } = useSelector((state) => state.themeReducer);
  const { attributes } = product;
  const attrWithImg = attributes.find((el) => el.hasImage);

  const renderSame = () => {
    return (
      <div>
        <p>All variants of this product has the same images</p>
      </div>
    );
  };

  const renderDiff = () => {
    const { variants } = product;
    const { values, key } = attrWithImg;
    const variantsByVal = values.map((val) => variants.find((el) => el[key] === val));
    return (
      <div className={`${style.container} ${!isLight && style.dark}`}>
        {values.map((val, i) => (
          <SingleVariantImage
            setProduct={setProduct}
            id={product._id}
            val={val}
            images={variantsByVal[i].images}
            key={i}
          />
        ))}
      </div>
    );
  };

  return <div>{attrWithImg ? renderDiff() : renderSame()}</div>;
};

VariantImages.propTypes = {
  product: Proptypes.object
};

export default VariantImages;
