// import { TextField } from '@mui/material';
// import React from 'react';
// import PropTypes from 'prop-types';
// import style from './style.module.scss';

// const CustomizeInput = ({
//   label,
//   value,
//   onChange,
//   error,
//   disabled,
//   size,
//   type,
//   multiline,
//   rows,
//   notFull
// }) => {
//   return (
//     <TextField
//       multiline={multiline}
//       className={style.input}
//       label={label}
//       value={value === undefined ? '' : value}
//       onChange={onChange}
//       disabled={disabled}
//       size={size}
//       error={error ? true : false}
//       helperText={error?.message}
//       type={type}
//       rows={rows}
//       fullWidth={!notFull}
//     />
//   );
// };

// CustomizeInput.propTypes = {
//   label: PropTypes.string,
//   value: PropTypes.string,
//   onChange: PropTypes.func,
//   error: PropTypes.object,
//   disabled: PropTypes.bool,
//   size: PropTypes.string,
//   type: PropTypes.string,
//   multiline: PropTypes.bool,
//   rows: PropTypes.number,
//   notFull: PropTypes.bool
// };

// export default CustomizeInput;

import React from 'react';
import PropTypes from 'prop-types';
import style from './style.module.scss';
import { useSelector } from 'react-redux';

const CustomizeInput = ({
  label,
  value,
  onChange,
  error,
  disabled,
  size,
  type,
  multiline,
  rows,
  fullWidth,
  placeholder
  // endAdornment
}) => {
  const { isLight } = useSelector((state) => state.themeReducer);
  return (
    <div className={style.control}>
      {label && <label>{label}</label>}
      {multiline ? (
        <textarea
          className={`${style.input} ${error && style.inputError} ${style[size]} ${
            fullWidth && style.full
          } ${!isLight && style.dark}`}
          value={value === undefined ? '' : value}
          onChange={onChange}
          disabled={disabled}
          size={size}
          type={type}
          rows={rows}
          placeholder={placeholder}
        />
      ) : (
        <input
          className={`${style.input} ${error && style.inputError} ${style[size]} ${
            fullWidth && style.full
          } ${!isLight && style.dark}`}
          value={value === undefined ? '' : value}
          onChange={onChange}
          disabled={disabled}
          size={size}
          type={type}
          rows={rows}
          placeholder={placeholder}
        />
      )}

      {error && <small className={style.error}>{error.message}</small>}
    </div>
  );
};

CustomizeInput.propTypes = {
  label: PropTypes.string,
  value: PropTypes.string,
  onChange: PropTypes.func,
  error: PropTypes.object,
  disabled: PropTypes.bool,
  size: PropTypes.string,
  type: PropTypes.string,
  multiline: PropTypes.bool,
  rows: PropTypes.number,
  fullWidth: PropTypes.bool,
  placeholder: PropTypes.string
  // endAdornment: PropTypes.element
};

export default CustomizeInput;
