import React from 'react';
import { createTheme, ThemeProvider } from '@mui/material';
import { useSelector } from 'react-redux';
import PropTypes from 'prop-types';

const ThemeContainer = ({ children }) => {
  const { isLight } = useSelector((state) => state.themeReducer);
  const shadow = isLight
    ? 'rgba(145, 158, 171, 0.2) 0px 0px 2px 0px, rgba(145, 158, 171, 0.12) 0px 12px 24px -4px'
    : 'rgba(0, 0, 0, 0.2) 0px 0px 2px 0px, rgba(0, 0, 0, 0.12) 0px 12px 24px -4px';
  const theme = createTheme({
    typography: {
      fontFamily: 'Montserrat, sans-serif'
    },
    shadows: [
      shadow,
      shadow,
      shadow,
      shadow,
      shadow,
      shadow,
      shadow,
      shadow,
      shadow,
      shadow,
      shadow,
      shadow,
      shadow,
      shadow
    ],
    palette: {
      mode: isLight ? 'light' : 'dark',
      primary: {
        main: '#3366FF',
        light: '#6690FF',
        dark: '#254EDB'
      },
      neutral: {
        main: isLight ? '#dfe3e8' : '#454f5b',
        dark: isLight ? '#c4cdd5' : '#637381'
      },
      danger: {
        main: '#d32f2f'
      },
      background: {
        paper: isLight ? 'white' : 'rgb(33, 43, 54)'
      }
    }
  });
  return <ThemeProvider theme={theme}>{children}</ThemeProvider>;
};

ThemeContainer.propTypes = {
  children: PropTypes.oneOfType([PropTypes.arrayOf(PropTypes.node), PropTypes.node]).isRequired
};

export default ThemeContainer;
