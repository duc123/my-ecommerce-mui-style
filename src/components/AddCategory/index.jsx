import { Modal } from '@mui/material';
import React, { useEffect } from 'react';
import PropTypes from 'prop-types';
import style from './style.module.scss';
import { useDispatch, useSelector } from 'react-redux';
import { yupResolver } from '@hookform/resolvers/yup';
import * as yup from 'yup';
import { Controller, useForm } from 'react-hook-form';
import { createCategory } from '../../redux/slices/category/api.js';
import CustomizeButton from '../CustomizeButton';
import CustomizeInput from '../CustomizeInput';

const AddCategory = ({ open, closeModal }) => {
  const { isLight } = useSelector((state) => state.themeReducer);
  const { loading } = useSelector((state) => state.categoryReducer);

  const dispatch = useDispatch();

  const schema = yup.object().shape({
    title: yup.string().required()
  });

  const {
    control,
    handleSubmit,
    formState: { isValid },
    setValue,
    reset
  } = useForm({
    resolver: yupResolver(schema),
    mode: 'all',
    defaultValues: {
      title: ''
    }
  });

  const resetState = () => {
    setValue('title', '');
    closeModal();
  };

  const submit = async (data) => {
    const res = await dispatch(createCategory(data));
    if (res.payload) {
      resetState();
    }
  };

  useEffect(() => {
    reset();
  }, [open]);

  return (
    <Modal onClose={closeModal} open={open}>
      <form
        onSubmit={handleSubmit(submit)}
        className={`${style.content} ${!isLight && style.dark}`}>
        <div className={style.header}>
          <h3>Add category</h3>
        </div>

        <Controller
          render={({ field: { value, onChange }, fieldState: { error } }) => (
            <CustomizeInput
              size="lg"
              label="Title"
              className={style.input}
              onChange={onChange}
              error={error}
              value={value}
              fullWidth
            />
          )}
          name="title"
          control={control}
        />

        <div className={style.footer}>
          <CustomizeButton
            type="button"
            variant="contained"
            onClick={closeModal}
            title="Cancel"
            size="small"
            color="neutral"
          />
          <CustomizeButton
            type="submit"
            variant="contained"
            title="Add"
            size="small"
            color="primary"
            loading={loading}
            disabled={loading}
          />
        </div>
      </form>
    </Modal>
  );
};

AddCategory.propTypes = {
  open: PropTypes.bool,
  closeModal: PropTypes.func
};

export default AddCategory;
