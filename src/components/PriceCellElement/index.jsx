import { CloseOutlined, EditOutlined } from '@mui/icons-material';
import { IconButton } from '@mui/material';
import React, { useEffect, useRef, useState } from 'react';
import CustomizeInput from '../CustomizeInput';
import style from './style.module.scss';
import PropTypes from 'prop-types';
import { editVariantInfo } from '../../api/product';
import Loading from '../Loading';

const InputElement = ({ price, onChange, onEdit }) => {
  const handleSubmit = (e) => {
    e.preventDefault();
    onEdit();
  };

  return (
    <form onSubmit={handleSubmit}>
      <CustomizeInput type="number" value={price} onChange={onChange} size="small" />
    </form>
  );
};

InputElement.propTypes = {
  price: PropTypes.number,
  onChange: PropTypes.func,
  onEdit: PropTypes.func
};

const Price = ({ price }) => {
  return <span>{price}</span>;
};

Price.propTypes = {
  price: PropTypes.number
};

const PriceCellElement = ({ product, setProduct, variant }) => {
  const [price, setPrice] = useState(null);

  const mainEl = useRef(null);

  const [inputVisible, setInputVisible] = useState(false);

  const showInput = () => setInputVisible(true);
  const hideInput = () => setInputVisible(false);

  const [loading, setLoading] = useState(false);

  const onChange = (e) => {
    setPrice(e.target.value);
  };

  const onEdit = async () => {
    setLoading(true);
    try {
      const newProduct = await editVariantInfo({ price: +price }, product._id, variant.id);
      setProduct(newProduct);
      setInputVisible(false);
    } catch (err) {
      console.log(err);
    }
    setLoading(false);
  };

  useEffect(() => {
    if (variant) setPrice(variant.price);
  }, [variant]);

  return (
    <div className={style.container} ref={mainEl}>
      {loading && <Loading loading={loading} />}
      <div className={style.main}>
        {inputVisible ? (
          <InputElement onEdit={onEdit} price={price} onChange={onChange} />
        ) : (
          <Price price={price} />
        )}
      </div>

      <p className={style.iconContainer}>
        {!inputVisible ? (
          <IconButton className={style.icon} onClick={showInput} size="small">
            <EditOutlined />
          </IconButton>
        ) : (
          <IconButton className={style.icon} onClick={hideInput} size="small">
            <CloseOutlined />
          </IconButton>
        )}
      </p>
    </div>
  );
};

PriceCellElement.propTypes = {
  product: PropTypes.object,
  setProduct: PropTypes.func,
  variant: PropTypes.object
};

export default PriceCellElement;
