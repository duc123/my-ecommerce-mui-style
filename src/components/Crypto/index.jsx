import React, { useEffect, useState } from 'react';
import axiosClient from '../../lib/axiosClient';
import {
  Chart as ChartJS,
  CategoryScale,
  LinearScale,
  PointElement,
  LineElement,
  Title,
  Tooltip,
  Legend
} from 'chart.js';
import { Line } from 'react-chartjs-2';
import { useSelector } from 'react-redux';

const Chart = ({ labels, datasets }) => {
  ChartJS.register(CategoryScale, LinearScale, PointElement, LineElement, Title, Tooltip, Legend);
  const options = {
    responsive: true,
    plugins: {
      legend: {
        position: 'top'
      },
      title: {
        display: true,
        text: 'Crypto price'
      }
    }
  };

  const data = {
    labels,
    datasets: [
      {
        label: 'Bitcoin price',
        data: datasets,
        fill: false,
        borderColor: '#3366ff',
        tension: 0.1
      }
    ]
  };

  return <Line data={data} options={options} />;
};

const Crypto = () => {
  const { crypto } = useSelector((state) => state.statisticReducer);

  const [datasets, setDatasets] = useState([]);
  const [labels, setLabels] = useState([]);

  const slice = 20;

  const transformData = () => {
    const times = crypto['Time Series (Digital Currency Daily)'];
    const dates = Object.keys(times).slice(0, slice);
    setLabels(dates);
    const values = Object.values(crypto['Time Series (Digital Currency Daily)'])
      .map((el) => el['4b. close (USD)'])
      .slice(0, slice);
    setLabels(dates);
    setDatasets(values);
  };

  useEffect(() => {
    if (crypto && crypto['Meta Data']) transformData();
  }, [crypto]);

  return <div>{datasets.length > 0 ? <Chart labels={labels} datasets={datasets} /> : ''}</div>;
};

export default Crypto;
