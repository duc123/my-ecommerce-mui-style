import { ButtonBase, Collapse } from '@mui/material';
import React, { useState } from 'react';
import { Link, useMatch, useResolvedPath } from 'react-router-dom';
import style from './style.module.scss';
import PropTypes from 'prop-types';
import {
  AttachMoneyOutlined,
  AttributionOutlined,
  AutoStoriesOutlined,
  BarChartOutlined,
  ExpandLessOutlined,
  ExpandMoreOutlined,
  HomeOutlined,
  PersonOutlined,
  StorefrontOutlined
} from '@mui/icons-material';
import { useSelector } from 'react-redux';

const NavItem = ({ item }) => {
  const { title, items } = item;

  const { isLight } = useSelector((state) => state.themeReducer);

  const renderSingle = () => {
    const path = useResolvedPath(`/${title}`);
    const match = useMatch({ path: path.pathname, end: true });
    return (
      <Link className={`${style.block} ${!isLight && style.dark}`} to={`/${title}`}>
        <ButtonBase className={style.baseBtn}>
          <div className={`${style.mainLink} ${match && style.active}`}>
            <div className={style.left}>
              {getIcon()}
              <span>{title}</span>
            </div>
          </div>
        </ButtonBase>
      </Link>
    );
  };

  const getIcon = () => {
    switch (title) {
      case 'users':
        return <PersonOutlined />;
      case 'category':
        return <AutoStoriesOutlined />;
      case 'products':
        return <StorefrontOutlined />;
      case 'orders':
        return <AttachMoneyOutlined />;
      case 'statistic':
        return <BarChartOutlined />;
      default:
        return <HomeOutlined />;
    }
  };

  const renderChildLink = (link) => {
    const path = useResolvedPath(`/${title}`);
    const match = useMatch({ path: path.pathname, end: true });
    return (
      <Link
        key={link}
        className={`${style.block} ${style.child} ${!isLight && style.dark}`}
        to={`/${link}`}>
        <ButtonBase className={style.baseBtn}>
          <div className={`${style.mainLink} ${match && style.active}`}>
            <span>{link}</span>
          </div>
        </ButtonBase>
      </Link>
    );
  };

  const renderList = () => {
    const [isExpand, setIsExpand] = useState(false);
    const toggleExpand = () => setIsExpand(() => !isExpand);
    return (
      <>
        <Link className={`${style.block} ${!isLight && style.dark}`} to="#">
          <ButtonBase onClick={toggleExpand} className={style.baseBtn}>
            <div className={`${style.mainLink}`}>
              <div className={style.left}>
                {getIcon()}
                <span>{title}</span>
              </div>
              {isExpand ? <ExpandLessOutlined /> : <ExpandMoreOutlined />}
            </div>
          </ButtonBase>
        </Link>
        <Collapse in={isExpand}>{items.map((link) => renderChildLink(link))}</Collapse>
      </>
    );
  };

  return (
    <>
      <div className={`${style.container}`}>{items ? renderList() : renderSingle()}</div>
    </>
  );
};

NavItem.propTypes = {
  item: PropTypes.object
};

export default NavItem;
