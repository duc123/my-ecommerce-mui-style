import React from 'react';
import PropTypes from 'prop-types';
import { useSelector } from 'react-redux';
import style from './style.module.scss';

const AttributeItem = ({ item }) => {
  const { isLight } = useSelector((state) => state.themeReducer);

  const { title } = item;

  return (
    <div className={`${isLight ? `componentBg` : `componentBgDark ${style.dark}`}`}>
      <h3 className={style.title}>{title}</h3>
    </div>
  );
};

AttributeItem.propTypes = {
  item: PropTypes.object
};

export default AttributeItem;
