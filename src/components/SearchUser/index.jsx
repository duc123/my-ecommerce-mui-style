import React from 'react';
import { useSelector } from 'react-redux';
import CustomizeAlert from '../CustomizeAlert';
import style from './style.module.scss';

const SearchUser = () => {
  const { isLight } = useSelector((state) => state.themeReducer);
  const alert = {
    severity: 'info',
    title: 'Function',
    content: 'Search function for user is not ready',
    variant: 'outlined'
  };
  return (
    <div className={`${style.container} ${!isLight && style.dark}`}>
      <CustomizeAlert fullWidth alert={alert} />
    </div>
  );
};

export default SearchUser;
