import { Collapse } from '@mui/material';
import React from 'react';
import style from './style.module.scss';
import PropTypes from 'prop-types';
import NavItem from '../NavItem';
import { DiamondOutlined } from '@mui/icons-material';
import { useSelector } from 'react-redux';

const Sidebar = ({ isExpand }) => {
  const { isLight } = useSelector((state) => state.themeReducer);

  const navItems = [
    {
      title: 'home'
    },
    {
      title: 'category'
    },
    {
      title: 'products'
    },
    {
      title: 'orders'
    },
    {
      title: 'users'
    },

    {
      title: 'statistic',
      items: ['Chart', 'Lie', 'Bar']
    }
  ];

  const renderNavItems = (list) => {
    return (
      <div>
        {list.map((item) => (
          <NavItem item={item} key={item.title} />
        ))}
      </div>
    );
  };

  return (
    <div className={`${style.sidebar} ${!isLight && style.dark}`}>
      <Collapse orientation="horizontal" in={isExpand}>
        <div className={style.container}>
          <div className={style.block}>
            <span className={style.title}>
              <DiamondOutlined />
            </span>
          </div>

          {renderNavItems(navItems)}
        </div>
      </Collapse>
    </div>
  );
};

Sidebar.propTypes = {
  isExpand: PropTypes.bool
};

export default Sidebar;
