// import React from 'react';
// import PropTypes from 'prop-types';
// import { FormControl, FormHelperText, InputLabel, MenuItem, Select } from '@mui/material';
// import style from './style.module.scss';

// const CustomizeSelect = ({
//   label,
//   value,
//   onChange,
//   error,
//   disabled,
//   size,
//   options,
//   multiple,
//   notFull
// }) => {
//   return (
//     <FormControl fullWidth={!notFull} error={error ? true : false}>
//       <InputLabel>{label}</InputLabel>
//       <Select
//         className={style.select}
//         sx={{ minWidth: '130px' }}
//         multiple={multiple}
//         value={value}
//         onChange={onChange}
//         label={label}
//         size={size}
//         disabled={disabled}>
//         {options.map((item) => (
//           <MenuItem sx={{ textTransform: 'capitalize' }} key={item} value={item}>
//             {item}
//           </MenuItem>
//         ))}
//       </Select>
//       {error && <FormHelperText>{error.message}</FormHelperText>}
//     </FormControl>
//   );
// };

// CustomizeSelect.propTypes = {
//   label: PropTypes.string,
//   value: PropTypes.string,
//   onChange: PropTypes.func,
//   error: PropTypes.object,
//   disabled: PropTypes.bool,
//   size: PropTypes.string,
//   options: PropTypes.array,
//   multiple: PropTypes.bool,
//   notFull: PropTypes.bool
// };

// export default CustomizeSelect;

import React from 'react';
import PropTypes from 'prop-types';
import {
  FormControl,
  FormHelperText,
  FormLabel,
  InputBase,
  MenuItem,
  Select,
  styled
} from '@mui/material';
import style from './style.module.scss';
import { useSelector } from 'react-redux';

const getPadding = (size) => {
  switch (size) {
    case 'md':
      return '7px 14px';
    case 'lg':
      return '10px 20px';
    default:
      return '8px 8px';
  }
};

const BootstrapInput = styled(InputBase)(({ theme, size, error }) => {
  const { isLight } = useSelector((state) => state.themeReducer);

  return {
    '& .MuiInputBase-input': {
      borderRadius: 4,
      position: 'relative',
      backgroundColor: theme.palette.background.paper,
      border: `1px solid ${error ? theme.palette.danger.main : isLight ? '#dfe3e8' : '#454f5b'}`,
      fontSize: size === 'md' ? '14px' : '13px',
      padding: getPadding(size),
      transition: theme.transitions.create(['border-color', 'box-shadow']),
      '&:focus': {
        borderColor: '#3391ff',
        boxShadow: `0 0 0 0.2rem rgba(51, 102, 255, ${isLight ? `0.5` : '0.8'})`
      }
    }
  };
});

const CustomizeSelect = ({
  value,
  onChange,
  error,
  disabled,
  size,
  options,
  multiple,
  full,
  placeholder,
  label
}) => {
  return (
    <FormControl fullWidth={full} error={error ? true : false}>
      {label && <label className={style.label}>{label}</label>}
      <Select
        multiple={multiple}
        displayEmpty
        renderValue={(selected) => {
          if (!selected) return <em>{placeholder ? placeholder : ''}</em>;
          return selected;
        }}
        value={value === undefined ? '' : value}
        onChange={onChange}
        size={size}
        disabled={disabled}
        input={<BootstrapInput size={size} error={error ? true : false} />}>
        {options.map((item) => (
          <MenuItem sx={{ textTransform: 'capitalize' }} key={item} value={item}>
            {item}
          </MenuItem>
        ))}
      </Select>
      {error && <span className={style.error}>{error.message}</span>}
    </FormControl>
  );
};

CustomizeSelect.propTypes = {
  label: PropTypes.string,
  value: PropTypes.string,
  onChange: PropTypes.func,
  error: PropTypes.object,
  disabled: PropTypes.bool,
  size: PropTypes.string,
  options: PropTypes.array,
  multiple: PropTypes.bool,
  full: PropTypes.bool,
  placeholder: PropTypes.string
};

export default CustomizeSelect;
