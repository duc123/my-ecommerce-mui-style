import React, { useEffect } from 'react';
import PropTypes from 'prop-types';
import Collapse from '@mui/material/Collapse';
import IconButton from '@mui/material/IconButton';
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableHead from '@mui/material/TableHead';
import TableRow from '@mui/material/TableRow';
import KeyboardArrowUpIcon from '@mui/icons-material/KeyboardArrowUp';
import KeyboardArrowDownIcon from '@mui/icons-material/KeyboardArrowDown';
import { useDispatch, useSelector } from 'react-redux';
import {
  ArrowForwardIosOutlined,
  DeleteOutline,
  DetailsOutlined,
  EditOutlined,
  MoreHorizOutlined
} from '@mui/icons-material';
import { Chip, ListItemIcon, ListItemText, Menu, MenuItem, Paper, Tooltip } from '@mui/material';
import { showModal } from '../../redux/slices/modal';
import { deleteProduct, getProducts } from '../../redux/slices/product/api';
import { selectProduct } from '../../redux/slices/product';
import CustomizePagination from '../CustomizePagination';
import { Link, useNavigate, useSearchParams } from 'react-router-dom';
import { LoadingButton } from '@mui/lab';
import style from './style.module.scss';
import Loading from '../Loading';
import { formatCurrency } from '../../utils/currency';

const getPublished = (isPublished) => {
  return isPublished > 0 ? (
    <Chip
      sx={{ fontSize: '10px' }}
      size="small"
      label="Published"
      variant="outlined"
      color="success"
    />
  ) : (
    <Chip
      sx={{ fontSize: '10px' }}
      size="small"
      label="Unpublished"
      variant="outlined"
      color="error"
    />
  );
};

function Row({ row }) {
  const { title, category, subCategory, productType, isPublished, _id: id, images, price } = row;
  const [open, setOpen] = React.useState(false);
  const dispatch = useDispatch();
  const { isLight } = useSelector((state) => state.themeReducer);

  const navigate = useNavigate();

  const onDelete = () => {
    const title = 'Delete product';
    const content = 'Do you want to delete this product? The data can not be recoverd once deleted';
    const callback = () => dispatch(deleteProduct(id));
    dispatch(showModal({ title, content, callback }));
  };

  return (
    <>
      <TableRow sx={{ '& > *': { borderBottom: 'unset' } }}>
        <TableCell className={style.cell}>
          <img className={style.img} src={images[0]} />
        </TableCell>
        <TableCell className={style.cell} component="th" scope="row">
          {title}
        </TableCell>
        <TableCell className={style.cell} align="right">
          {formatCurrency(price)}$
        </TableCell>
        <TableCell className={style.cell} align="right">
          {category}
        </TableCell>
        <TableCell className={style.cell} align="right">
          {subCategory}
        </TableCell>
        <TableCell className={style.cell} align="right">
          {productType}
        </TableCell>
        <TableCell className={style.cell} align="right">
          {getPublished(isPublished)}
        </TableCell>
        <TableCell className={style.cell} align="right">
          <Link to={`/products/${id}`}>
            <IconButton size="small">
              <ArrowForwardIosOutlined sx={{ fontSize: '17px' }} />
            </IconButton>
          </Link>
        </TableCell>
      </TableRow>
    </>
  );
}

Row.propTypes = {
  row: PropTypes.object
};

export default function ProductList() {
  const { isLight } = useSelector((state) => state.themeReducer);

  const { productData, loading, searchQueries } = useSelector((state) => state.productReducer);

  const navigate = useNavigate();
  const dispatch = useDispatch();

  const [searchParams] = useSearchParams();

  const currentPage = searchParams.get('page') ? +searchParams.get('page') : 1;

  const onPageChange = (event, value) => {
    navigate(`/products?page=${value}`);
  };

  useEffect(() => {
    let stop = false;
    const { category, subCategory, productType, price } = searchQueries;
    if (!stop) {
      dispatch(
        getProducts({
          page: currentPage,
          category,
          subCategory,
          productType,
          sort: price
        })
      );
    }
    return () => (stop = true);
  }, [currentPage, searchQueries]);

  const renderPagination = () => {
    const { pageCount } = productData;

    return (
      <CustomizePagination
        onChange={onPageChange}
        disabled={false}
        count={pageCount}
        page={currentPage}
        // a
      />
    );
  };

  return (
    <>
      <Loading loading={loading} />

      <TableContainer className={`${style.container} ${!isLight && style.dark}`}>
        <Table
          // size="small"
          aria-label="collapsible table">
          <TableHead>
            <TableRow>
              <TableCell className={style.cellHead}>Image</TableCell>
              <TableCell className={style.cellHead}>Title</TableCell>
              <TableCell align="right" className={style.cellHead}>
                Price
              </TableCell>

              <TableCell className={style.cellHead} align="right">
                Category
              </TableCell>
              <TableCell className={style.cellHead} align="right">
                Subcategory
              </TableCell>
              <TableCell className={style.cellHead} align="right">
                Type
              </TableCell>
              <TableCell className={style.cellHead} align="right">
                Status
              </TableCell>
              <TableCell className={style.cellHead} align="right"></TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {productData?.products?.map((row) => (
              <Row key={row.title} row={row} />
            ))}
          </TableBody>
        </Table>
        <div style={{ padding: '10px' }}>{productData?.products && renderPagination()}</div>
      </TableContainer>
    </>
  );
}
