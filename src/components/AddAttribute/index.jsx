import { Modal } from '@mui/material';
import React, { useEffect } from 'react';
import PropTypes from 'prop-types';
import style from './style.module.scss';
import { useDispatch, useSelector } from 'react-redux';
import { yupResolver } from '@hookform/resolvers/yup';
import * as yup from 'yup';
import { Controller, useForm } from 'react-hook-form';
import CustomizeButton from '../CustomizeButton';
import CustomizeInput from '../CustomizeInput';
import { addAttribute } from '../../redux/slices/attributes/attributeActions';

const AddAttribute = ({ open, closeModal }) => {
  const { isLight } = useSelector((state) => state.themeReducer);
  const { loading } = useSelector((state) => state.attributeReducer);

  const dispatch = useDispatch();

  const schema = yup.object().shape({
    title: yup.string().required()
  });

  const {
    control,
    handleSubmit,
    formState: { isValid },
    setValue,
    reset
  } = useForm({
    resolver: yupResolver(schema),
    mode: 'all',
    defaultValues: {
      title: ''
    }
  });

  const resetState = () => {
    setValue('title', '');
    closeModal();
  };

  const submit = async (data) => {
    const res = await dispatch(addAttribute(data));
    if (res.payload) {
      resetState();
    }
  };

  useEffect(() => {
    reset();
  }, [open]);

  return (
    <Modal onClose={closeModal} open={open}>
      <form
        onSubmit={handleSubmit(submit)}
        className={`${style.content} ${!isLight && style.dark}`}
      >
        <div className={style.header}>
          <h3>Add Attribute</h3>
        </div>

        <Controller
          render={({ field: { value, onChange }, fieldState: { error } }) => (
            <CustomizeInput
              size="small"
              label="Title"
              className={style.input}
              onChange={onChange}
              error={error}
              value={value}
            />
          )}
          name="title"
          control={control}
        />

        <div className={style.footer}>
          <CustomizeButton
            type="button"
            variant="contained"
            onClick={closeModal}
            title="Cancel"
            size="medium"
            color="neutral"
          />
          <CustomizeButton
            type="submit"
            variant="contained"
            title="Add"
            size="medium"
            color="primary"
            loading={loading}
            disabled={loading}
          />
        </div>
      </form>
    </Modal>
  );
};

AddAttribute.propTypes = {
  open: PropTypes.bool,
  closeModal: PropTypes.func
};

export default AddAttribute;
