import React from 'react';
import { useSelector } from 'react-redux';
import { useSearchParams } from 'react-router-dom';
import AddComplexProduct from '../AddComplexProduct';
import AddSimpleProduct from '../AddSimpleProduct';
import Loading from '../Loading';

const AddProduct = () => {
  const [searchParams] = useSearchParams();

  const paramType = searchParams.get('type');

  const type = paramType === 'complex' ? paramType : 'simple';

  const { loading } = useSelector((state) => state.productReducer);

  return (
    <div>
      <Loading loading={loading} />
      {type === 'simple' ? <AddSimpleProduct /> : <AddComplexProduct />}
    </div>
  );
};

export default AddProduct;
