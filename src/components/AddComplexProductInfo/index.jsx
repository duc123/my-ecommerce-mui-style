import { Grid, IconButton, Modal } from '@mui/material';
import React, { useEffect, useState } from 'react';
import PropTypes from 'prop-types';
import style from './style.module.scss';
import { useDispatch, useSelector } from 'react-redux';
import { yupResolver } from '@hookform/resolvers/yup';
import * as yup from 'yup';
import { Controller, useForm } from 'react-hook-form';
import CustomizeButton from '../CustomizeButton';
import CustomizeInput from '../CustomizeInput';
import { CloseOutlined, UploadOutlined } from '@mui/icons-material';
import CustomizeSelect from '../CustomizeSelect';
import { fileListToBase64 } from '../../utils/toBase64';
import { nextStep, saveComplexData } from '../../redux/slices/product';

const AddComplexProductInfo = () => {
  const { isLight } = useSelector((state) => state.themeReducer);
  const { loading, complexData } = useSelector((state) => state.productReducer);
  const { categories } = useSelector((state) => state.categoryReducer);

  const [categoryList, setCategoryList] = useState([]);
  const [subcategoryList, setSubCategoryList] = useState([]);

  const [displayedImages, setDisplayedImages] = useState([]);

  const dispatch = useDispatch();

  const schema = yup.object().shape({
    title: yup.string().required(),
    price: yup
      .number()
      .required()
      .nullable(true)
      .transform((_, val) => (val ? Number(val) : null)),
    category: yup.string().required(),
    subCategory: yup.string().required(),
    brand: yup.string(),

    description: yup.string().required(),
    images: yup.mixed().test('images', 'Images are required', (values) => {
      return values?.length > 0;
    })
  });

  const { control, handleSubmit, setValue, watch } = useForm({
    resolver: yupResolver(schema),
    mode: 'all',
    defaultValues: {
      title: '',
      category: '',
      subCategory: '',
      description: '',
      brand: '',
      price: ''
    }
  });

  const watchCategory = watch('category');

  const watchImages = watch('images');

  const submit = (data) => {
    dispatch(saveComplexData(data));
    dispatch(nextStep());
  };

  // Map to get category title for select
  useEffect(() => {
    if (categories?.length > 0) setCategoryList(categories.map((el) => el.title));
  }, [categories]);

  //   Get subcategory list for select
  useEffect(() => {
    if (categories.length > 0 && watchCategory) {
      const currentSubs = categories.find((el) => el.title === watchCategory).subs;
      setSubCategoryList(currentSubs);
    } else {
      setSubCategoryList([]);
    }
  }, [watchCategory, categories]);

  useEffect(() => {
    if (!complexData) {
      setValue('subCategory', '');
    } else {
      if (complexData.category !== watchCategory) setValue('subCategory', '');
    }
  }, [watchCategory, complexData]);

  //   Set value when edit
  useEffect(() => {
    if (complexData) {
      setValue('title', complexData.title);
      setValue('category', complexData.category);

      setValue('subCategory', complexData.subCategory);

      setValue('description', complexData.description);

      setValue('brand', complexData.brand);

      setValue('images', complexData.images);
    }
  }, [complexData]);

  console.log(complexData);

  useEffect(() => {
    if (watchImages?.length > 0) {
      fileListToBase64(watchImages).then((values) => {
        setDisplayedImages(values);
      });
    } else {
      setDisplayedImages([]);
    }
  }, [watchImages]);

  return (
    <form onSubmit={handleSubmit(submit)} className={`${style.content} ${!isLight && style.dark}`}>
      <Grid container spacing={3}>
        <Grid item xs={12} md={6}>
          <Controller
            render={({ field: { value, onChange }, fieldState: { error } }) => (
              <CustomizeInput
                size="medium"
                label="Title"
                className={style.input}
                onChange={onChange}
                error={error}
                value={value}
                type="text"
              />
            )}
            name="title"
            control={control}
          />
        </Grid>
        <Grid item xs={12} md={6}>
          <Controller
            render={({ field: { value, onChange }, fieldState: { error } }) => (
              <CustomizeInput
                size="medium"
                label="Price"
                className={style.input}
                onChange={onChange}
                error={error}
                value={value}
                type="number"
              />
            )}
            name="price"
            control={control}
          />
        </Grid>
        <Grid item xs={12} md={6}>
          <Controller
            render={({ field: { value, onChange }, fieldState: { error } }) => (
              <CustomizeInput
                size="medium"
                label="Brand (optional)"
                className={style.input}
                onChange={onChange}
                error={error}
                value={value}
                type="text"
              />
            )}
            name="brand"
            control={control}
          />
        </Grid>

        <Grid xs={12} item md={6}>
          <Controller
            render={({ field: { value, onChange }, fieldState: { error } }) => (
              <CustomizeSelect
                size="medium"
                label="Category"
                className={style.input}
                onChange={onChange}
                error={error}
                value={value}
                options={categoryList}
              />
            )}
            name="category"
            control={control}
          />
        </Grid>

        <Grid xs={12} item md={6}>
          <Controller
            render={({ field: { value, onChange }, fieldState: { error } }) => (
              <CustomizeSelect
                size="medium"
                label="Subcategory"
                className={style.input}
                onChange={onChange}
                error={error}
                value={value}
                options={subcategoryList}
              />
            )}
            name="subCategory"
            control={control}
          />
        </Grid>
        <Grid item xs={12}>
          <Controller
            render={({ field: { value, onChange }, fieldState: { error } }) => (
              <CustomizeInput
                multiline={true}
                size="medium"
                label="Description"
                className={style.input}
                onChange={onChange}
                error={error}
                value={value}
                rows={4}
              />
            )}
            name="description"
            control={control}
          />
        </Grid>
        <Grid item xs={12}>
          <label htmlFor="contained-button-file">
            <Controller
              defaultValue=""
              render={({ field: { onChange, value }, fieldState: { error } }) => {
                return (
                  <>
                    <input
                      style={{ display: 'none' }}
                      accept="image/*"
                      type="file"
                      value={value.files}
                      onChange={(e) => {
                        onChange(e.target.files);
                      }}
                      placeholder="Images"
                      id="contained-button-file"
                      multiple
                    />

                    <div className={style.uploadBlock}>
                      <div className={`${style.upload} ${!isLight && style.dark}`}>
                        <p>Upload images</p>
                        <UploadOutlined />
                      </div>
                      <small className={style.error}>{error && error.message}</small>
                    </div>
                  </>
                );
              }}
              name="images"
              control={control}
            />
          </label>
          {displayedImages.length > 0 ? (
            <div className={style.images}>
              {displayedImages.map((img, i) => (
                <img key={i} src={img} />
              ))}
            </div>
          ) : (
            ''
          )}
        </Grid>
      </Grid>
      <div className={style.footer}>
        <CustomizeButton
          title="Next step"
          type="submit"
          color="primary"
          variant="contained"
          size="medium"
        />
      </div>
    </form>
  );
};

export default AddComplexProductInfo;
