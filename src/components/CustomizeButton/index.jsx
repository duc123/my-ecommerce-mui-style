import React from 'react';
import PropTypes from 'prop-types';
import style from './style.module.scss';
import { useSelector } from 'react-redux';
import { LoadingButton } from '@mui/lab';

const CustomizeButton = ({
  type,
  title,
  variant,
  size,
  onClick,
  endIcon,
  color,
  disabled,
  loading,
  component,
  fullWidth
}) => {
  const { isLight } = useSelector((state) => state.themeReducer);

  return (
    <LoadingButton
      fullWidth={fullWidth}
      type={type}
      variant={variant}
      size={size}
      endIcon={endIcon}
      onClick={onClick}
      color={color}
      className={`${style.btn} ${style[size]}`}
      disabled={disabled}
      loading={loading}
      component={component}>
      {title}
    </LoadingButton>
  );
};

CustomizeButton.propTypes = {
  type: PropTypes.string,
  title: PropTypes.string,
  variant: PropTypes.string,
  size: PropTypes.string,
  onClick: PropTypes.func,
  endIcon: PropTypes.element,
  color: PropTypes.string,
  disabled: PropTypes.bool,
  loading: PropTypes.bool,
  component: PropTypes.string
};

export default CustomizeButton;
