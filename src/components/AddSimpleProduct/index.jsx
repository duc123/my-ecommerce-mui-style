import { Grid, IconButton, Modal } from '@mui/material';
import React, { useEffect, useState } from 'react';
import PropTypes from 'prop-types';
import style from './style.module.scss';
import { useDispatch, useSelector } from 'react-redux';
import { yupResolver } from '@hookform/resolvers/yup';
import * as yup from 'yup';
import { Controller, useForm } from 'react-hook-form';
import CustomizeButton from '../../components/CustomizeButton';
import CustomizeInput from '../../components/CustomizeInput';
import { CloseOutlined, UploadOutlined } from '@mui/icons-material';
import CustomizeSelect from '../../components/CustomizeSelect';
import { addProduct } from '../../redux/slices/product/api';
import { fileListToBase64 } from '../../utils/toBase64';
import { useNavigate } from 'react-router-dom';

const AddSimpleProduct = () => {
  const { isLight } = useSelector((state) => state.themeReducer);
  const { loading, selectedProduct } = useSelector((state) => state.productReducer);
  const { categories } = useSelector((state) => state.categoryReducer);

  const [categoryList, setCategoryList] = useState([]);
  const [subcategoryList, setSubCategoryList] = useState([]);

  const [displayedImages, setDisplayedImages] = useState([]);

  const dispatch = useDispatch();
  const navigate = useNavigate();

  const schema = yup.object().shape({
    title: yup.string().required(),
    price: yup
      .number()
      .required()
      .nullable(true)
      .transform((_, val) => (val ? Number(val) : null)),
    category: yup.string().required(),
    subCategory: yup.string().required(),
    brand: yup.string(),
    stock: yup
      .number()
      .required()
      .nullable(true)
      .transform((_, val) => (val ? Number(val) : null)),
    description: yup.string().required(),
    images: yup.mixed().test('images', 'Images are required', (values) => {
      return values?.length > 0;
    })
  });

  const { control, handleSubmit, setValue, watch } = useForm({
    resolver: yupResolver(schema),
    mode: 'all',
    defaultValues: {
      title: '',
      category: '',
      subCategory: '',
      description: '',
      brand: '',
      productType: 'simple'
    }
  });

  const watchCategory = watch('category');

  const watchImages = watch('images');

  const submit = (data) => {
    if (selectedProduct) {
      onEdit(data);
    } else {
      onAdd(data);
    }
  };

  const onAdd = async (data) => {
    const formData = new FormData();
    const keys = Object.keys(data);
    for (let key of keys) {
      if (key !== 'images') formData.append(key, data[key]);
    }
    for (let file of data.images) {
      formData.append('images', file);
    }
    const res = await dispatch(addProduct(formData));
    if (res.payload) navigate('/products');
  };

  const onEdit = async (para) => {
    const data = { id: selectedProduct._id, data: para };
  };

  // Map to get category title for select
  useEffect(() => {
    if (categories?.length > 0) setCategoryList(categories.map((el) => el.title));
  }, [categories]);

  //   Get subcategory list for select
  useEffect(() => {
    if (categories.length > 0 && watchCategory) {
      const currentSubs = categories.find((el) => el.title === watchCategory).subs;
      setSubCategoryList(currentSubs);
    } else {
      setSubCategoryList([]);
    }
  }, [watchCategory, categories]);

  useEffect(() => {
    if (!selectedProduct) {
      setValue('subCategory', '');
    } else {
      if (selectedProduct.category !== watchCategory) setValue('subCategory', '');
    }
  }, [watchCategory, selectedProduct]);

  //   Set value when edit
  useEffect(() => {
    if (selectedProduct) {
      setValue('title', selectedProduct.title);
      setValue('category', selectedProduct.category);

      setValue('subCategory', selectedProduct.subCategory);

      setValue('description', selectedProduct.description);

      setValue('brand', selectedProduct.brand);
    }
  }, [selectedProduct, open]);

  useEffect(() => {
    if (watchImages?.length > 0) {
      fileListToBase64(watchImages).then((values) => {
        setDisplayedImages(values);
      });
    } else {
      setDisplayedImages([]);
    }
  }, [watchImages]);

  return (
    <form onSubmit={handleSubmit(submit)} className={`${style.content} ${!isLight && style.dark}`}>
      <div className={style.block}>
        <h3>Genral information</h3>
        <Grid container spacing={3}>
          <Grid item xs={12} md={6}>
            <Controller
              render={({ field: { value, onChange }, fieldState: { error } }) => (
                <CustomizeInput
                  fullWidth
                  size="lg"
                  label="Title"
                  className={style.input}
                  onChange={onChange}
                  error={error}
                  value={value}
                  type="text"
                  placeholder="Ex: Macbook pro"
                />
              )}
              name="title"
              control={control}
            />
          </Grid>
          <Grid item xs={12} md={6}>
            <Controller
              render={({ field: { value, onChange }, fieldState: { error } }) => (
                <CustomizeInput
                  fullWidth
                  size="lg"
                  label="Price"
                  className={style.input}
                  onChange={onChange}
                  error={error}
                  value={value}
                  type="number"
                  placeholder="Ex: 50"
                />
              )}
              name="price"
              control={control}
            />
          </Grid>
          <Grid item xs={12} md={6}>
            <Controller
              render={({ field: { value, onChange }, fieldState: { error } }) => (
                <CustomizeInput
                  fullWidth
                  size="lg"
                  label="Brand (optional)"
                  className={style.input}
                  onChange={onChange}
                  error={error}
                  value={value}
                  type="text"
                  placeholder="Ex: Apple"
                />
              )}
              name="brand"
              control={control}
            />
          </Grid>
          <Grid item xs={12} md={6}>
            <Controller
              render={({ field: { value, onChange }, fieldState: { error } }) => (
                <CustomizeInput
                  fullWidth
                  size="lg"
                  label="Stock"
                  className={style.input}
                  onChange={onChange}
                  error={error}
                  value={value}
                  type="number"
                  placeholder="Ex: 67"
                />
              )}
              name="stock"
              control={control}
            />
          </Grid>
          <Grid xs={12} item md={6}>
            <Controller
              render={({ field: { value, onChange }, fieldState: { error } }) => (
                <CustomizeSelect
                  size="lg"
                  label="Category"
                  className={style.input}
                  onChange={onChange}
                  error={error}
                  value={value}
                  options={categoryList}
                  full
                />
              )}
              name="category"
              control={control}
            />
          </Grid>

          <Grid xs={12} item md={6}>
            <Controller
              render={({ field: { value, onChange }, fieldState: { error } }) => (
                <CustomizeSelect
                  size="lg"
                  label="Subcategory"
                  className={style.input}
                  onChange={onChange}
                  error={error}
                  value={value}
                  options={subcategoryList}
                  full
                />
              )}
              name="subCategory"
              control={control}
            />
          </Grid>
          <Grid item xs={12}>
            <Controller
              render={({ field: { value, onChange }, fieldState: { error } }) => (
                <CustomizeInput
                  fullWidth
                  multiline={true}
                  size="lg"
                  label="Description"
                  className={style.input}
                  onChange={onChange}
                  error={error}
                  value={value}
                  rows={6}
                  placeholder="Ex: This product is ..."
                />
              )}
              name="description"
              control={control}
            />
          </Grid>
        </Grid>
      </div>

      <div className={style.block}>
        <h3>Images</h3>
        <label htmlFor="contained-button-file">
          <Controller
            defaultValue=""
            render={({ field: { onChange, value }, fieldState: { error } }) => {
              return (
                <>
                  <input
                    style={{ display: 'none' }}
                    accept="image/*"
                    type="file"
                    value={value.files}
                    onChange={(e) => {
                      onChange(e.target.files);
                    }}
                    placeholder="Images"
                    id="contained-button-file"
                    multiple
                  />

                  <div className={style.uploadBlock}>
                    <div className={`${style.upload} ${!isLight && style.dark}`}>
                      <p>Upload images</p>
                      <UploadOutlined />
                    </div>
                    <small className={style.error}>{error && error.message}</small>
                  </div>
                </>
              );
            }}
            name="images"
            control={control}
          />
        </label>
        {displayedImages.length > 0 ? (
          <div className={style.images}>
            {displayedImages.map((img, i) => (
              <img key={i} src={img} />
            ))}
          </div>
        ) : (
          ''
        )}
      </div>

      <div className={style.footer}>
        <CustomizeButton
          type="submit"
          variant="contained"
          title="Save"
          size="lg"
          color="primary"
          loading={loading}
          disabled={loading}
        />
      </div>
    </form>
  );
};

export default AddSimpleProduct;
