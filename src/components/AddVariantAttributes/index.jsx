import React from 'react';
import { useDispatch } from 'react-redux';
import { prevStep } from '../../redux/slices/product';
import CustomizeButton from '../CustomizeButton';
import style from './style.module.scss';

const AddVariantAttributes = () => {
  const dispatch = useDispatch();

  const onPrev = () => dispatch(prevStep());

  return (
    <div className={style.container}>
      <CustomizeButton
        onClick={onPrev}
        title="Previous"
        type="button"
        color="neutral"
        variant="contained"
        size="medium"
      />

      <CustomizeButton
        title="Next"
        type="button"
        color="primary"
        variant="contained"
        size="medium"
      />
    </div>
  );
};

export default AddVariantAttributes;
