import React, { useEffect } from 'react';
import PropTypes from 'prop-types';
import IconButton from '@mui/material/IconButton';
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableHead from '@mui/material/TableHead';
import TableRow from '@mui/material/TableRow';
import { useDispatch, useSelector } from 'react-redux';
import {
  ArrowForwardIosOutlined,
  DeleteOutline,
  DetailsOutlined,
  MoreHorizOutlined
} from '@mui/icons-material';
import { Chip, Menu } from '@mui/material';

import CustomizePagination from '../CustomizePagination';
import { Link, useNavigate, useSearchParams } from 'react-router-dom';
import style from './style.module.scss';
import { getUsers } from '../../redux/slices/user/api';
import { getOrders } from '../../redux/slices/order/api';
import Loading from '../Loading';

const getPaymentStatus = (isActive) => {
  return isActive ? (
    <Chip sx={{ fontSize: '10px' }} size="small" label="Paid" variant="outlined" color="success" />
  ) : (
    <Chip sx={{ fontSize: '10px' }} size="small" label="Unpaid" variant="outlined" color="error" />
  );
};

const getDeliveryStatus = (isActive) => {
  return isActive ? (
    <Chip sx={{ fontSize: '10px' }} size="small" label="Done" variant="outlined" color="success" />
  ) : (
    <Chip
      sx={{ fontSize: '10px' }}
      size="small"
      label="Processing"
      variant="outlined"
      color="error"
    />
  );
};

function Row({ row }) {
  const {
    title,
    price,
    image,
    paymentStatus,
    deliveryStatus,
    attributes,
    variant,
    quantity,
    _id: id
  } = row;
  const [open, setOpen] = React.useState(false);
  const dispatch = useDispatch();
  const { isLight } = useSelector((state) => state.themeReducer);

  const navigate = useNavigate();

  const menuPopover = () => {
    const [anchorEl, setAnchorEl] = React.useState(null);
    const openMenu = Boolean(anchorEl);
    const handleClick = (event) => {
      setAnchorEl(event.currentTarget);
    };
    const handleClose = () => {
      setAnchorEl(null);
    };
    const onDelete = () => {};

    const onDetail = () => {
      navigate(`/orders/${id}`);
    };

    return (
      <>
        <IconButton
          id="basic-button"
          aria-controls={openMenu ? 'basic-menu' : undefined}
          aria-haspopup="true"
          aria-expanded={openMenu ? 'true' : undefined}
          onClick={handleClick}>
          <MoreHorizOutlined />
        </IconButton>
        <Menu
          id="basic-menu"
          anchorEl={anchorEl}
          open={openMenu}
          onClose={handleClose}
          MenuListProps={{
            'aria-labelledby': 'basic-button'
          }}>
          <div className={`${style.menu} ${!isLight && style.dark}`}>
            <div onClick={onDetail} className={style.menuItem}>
              <span>Detail</span>
              <DetailsOutlined />
            </div>
            <div className={style.menuItem} onClick={onDelete}>
              <span>Delete</span>
              <DeleteOutline />
            </div>
          </div>
        </Menu>
      </>
    );
  };

  return (
    <>
      <TableRow sx={{ '& > *': { borderBottom: 'unset' } }}>
        <TableCell className={style.cell}>
          {image ? <img className={style.img} src={image} /> : 'No avatar'}
        </TableCell>
        <TableCell className={style.cell} component="th" align="right" scope="row">
          {title}
        </TableCell>
        <TableCell className={style.cell} align="right">
          {price}$
        </TableCell>
        <TableCell className={style.cell} align="right">
          {quantity}
        </TableCell>
        <TableCell className={style.cell} align="right">
          {getPaymentStatus(paymentStatus)}
        </TableCell>
        <TableCell className={style.cell} align="right">
          {getDeliveryStatus(deliveryStatus)}
        </TableCell>
        <TableCell className={style.cell} align="right">
          <Link to={`/orders/${id}`}>
            <IconButton size="small">
              <ArrowForwardIosOutlined sx={{ fontSize: '17px' }} />
            </IconButton>
          </Link>
        </TableCell>
      </TableRow>
    </>
  );
}

Row.propTypes = {
  row: PropTypes.object
};

export default function OrderList() {
  const { isLight } = useSelector((state) => state.themeReducer);

  const { orderData, loading } = useSelector((state) => state.orderReducer);

  const navigate = useNavigate();
  const dispatch = useDispatch();

  const [searchParams] = useSearchParams();

  const currentPage = searchParams.get('p') ? +searchParams.get('p') : 1;

  const onPageChange = (event, value) => {
    navigate(`/orders?p=${value}`);
  };

  useEffect(() => {
    let stop = false;
    if (!stop) dispatch(getOrders({ p: currentPage }));
    return () => (stop = true);
  }, [currentPage]);

  const renderPagination = () => {
    const { pageCount } = orderData;

    return (
      <CustomizePagination
        onChange={onPageChange}
        disabled={false}
        count={pageCount}
        page={currentPage}
        // a
      />
    );
  };

  return (
    <>
      <Loading loading={loading} />
      <TableContainer className={`${style.container} ${!isLight && style.dark}`}>
        <Table aria-label="collapsible table">
          <TableHead>
            <TableRow>
              <TableCell className={style.cellHead}>Image</TableCell>

              <TableCell align="right" className={style.cellHead}>
                Title
              </TableCell>
              <TableCell align="right" className={style.cellHead}>
                Price
              </TableCell>
              <TableCell className={style.cellHead} align="right">
                Quantity
              </TableCell>

              <TableCell className={style.cellHead} align="right">
                Payment status
              </TableCell>
              <TableCell className={style.cellHead} align="right">
                Delivery status
              </TableCell>
              <TableCell className={style.cellHead} align="right"></TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {orderData?.orders?.map((row, i) => (
              <Row key={i} row={row} />
            ))}
          </TableBody>
        </Table>
        <div style={{ padding: '10px' }}>{orderData?.orders && renderPagination()}</div>
      </TableContainer>
    </>
  );
}
