import React from 'react';
import style from './style.module.scss';

const Loading = ({ loading }) => {
  return loading ? (
    <div className={style.container}>
      <div className={style.loading}></div>
    </div>
  ) : (
    ''
  );
};

export default Loading;
