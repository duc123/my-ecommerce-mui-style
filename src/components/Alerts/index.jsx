import React from 'react';
import { useSelector } from 'react-redux';
import CustomizeAlert from '../CustomizeAlert';
import style from './style.module.scss';

const Alerts = () => {
  const renderList = () => {
    return (
      <>
        {alerts.map((el) => (
          <CustomizeAlert key={el.id} alert={el} />
        ))}
      </>
    );
  };

  const { alerts } = useSelector((state) => state.alertReducer);
  return alerts?.length > 0 ? <div className={style.alerts}>{renderList()}</div> : '';
};

export default Alerts;
