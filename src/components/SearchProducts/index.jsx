import React, { useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useNavigate } from 'react-router-dom';
import { resetSearch, setQueries } from '../../redux/slices/product';
import CustomizeButton from '../CustomizeButton';
import CustomizeSelect from '../CustomizeSelect';
import style from './style.module.scss';

const SearchProducts = () => {
  const { categories } = useSelector((state) => state.categoryReducer);

  const { isLight } = useSelector((state) => state.themeReducer);

  const dispatch = useDispatch();
  const navigate = useNavigate();

  const {
    searchQueries: { category: c, subCategory: sC, productType: pT, price: pS }
  } = useSelector((state) => state.productReducer);

  const [controlledCategory, setControlledCategory] = useState(c);
  const [controlledSubC, setControlledSubC] = useState(sC);

  const [controlledProductT, setControlledProductT] = useState(pT);

  const [controlledPrice, setControlledPrice] = useState(pS);

  const handleCategoryChange = (e) => {
    setControlledCategory(e.target.value);
    setControlledSubC('all');
  };
  const handleSubChange = (e) => {
    setControlledSubC(e.target.value);
  };
  const handleTypeChange = (e) => {
    setControlledProductT(e.target.value);
  };
  const handlePriceSortChange = (e) => {
    setControlledPrice(e.target.value);
  };

  const getOptions = () => {
    const list = categories.map((el) => el.title);
    return [...list, 'all'];
  };

  const getSubOptions = () => {
    console.log(controlledCategory);
    const list = categories.find((el) => el.title === controlledCategory)?.subs;
    return [...list, 'all'];
  };

  const onSearch = () => {
    const queries = {
      category: controlledCategory,
      subCategory: controlledSubC,
      price: controlledPrice,
      productType: controlledProductT
    };
    dispatch(setQueries(queries));
    navigate(`/products/?page=${1}`);
  };

  const onReset = () => {
    dispatch(resetSearch());
    navigate(`/products/?page=${1}`);
  };

  return (
    <div className={`${style.container} ${!isLight && style.dark}`}>
      <div className={style.left}>
        <div className={style.item}>
          <CustomizeSelect
            value={controlledPrice}
            onChange={handlePriceSortChange}
            error={null}
            size="sm"
            options={['high to low', 'low to high', 'default']}
            full
            label="Price"
          />
        </div>
        <div className={style.item}>
          <CustomizeSelect
            value={controlledProductT}
            onChange={handleTypeChange}
            error={null}
            size="sm"
            options={['all', 'simple', 'complex']}
            full
            label="Type"
          />
        </div>
        <div className={style.item}>
          {categories?.length > 0 && (
            <CustomizeSelect
              label="Category"
              value={controlledCategory}
              onChange={handleCategoryChange}
              error={null}
              size="sm"
              options={getOptions()}
              full
            />
          )}
        </div>
        {controlledCategory !== 'all' && (
          <div className={style.item}>
            <CustomizeSelect
              value={controlledSubC}
              onChange={handleSubChange}
              error={null}
              size="sm"
              options={getSubOptions()}
              full
              label="Subcategory"
            />
          </div>
        )}
      </div>
      <div className={style.right}>
        <CustomizeButton
          title="Resest"
          size="sm"
          type="button"
          color="neutral"
          variant="contained"
          onClick={onReset}
        />
        <CustomizeButton
          title="Search"
          size="sm"
          type="button"
          color="primary"
          variant="contained"
          onClick={onSearch}
        />
      </div>
    </div>
  );
};

export default SearchProducts;
