import { Modal } from '@mui/material';
import React, { useEffect } from 'react';
import PropTypes from 'prop-types';
import style from './style.module.scss';
import { useDispatch, useSelector } from 'react-redux';
import { yupResolver } from '@hookform/resolvers/yup';
import * as yup from 'yup';
import { Controller, useForm } from 'react-hook-form';
import CustomizeButton from '../CustomizeButton';
import CustomizeInput from '../CustomizeInput';
import { addSubCategory } from '../../redux/slices/category/api.js';

const AddSubCategory = ({ open, closeModal, category }) => {
  const { isLight } = useSelector((state) => state.themeReducer);
  const { loading } = useSelector((state) => state.categoryReducer);

  const { _id: id, title } = category;

  const dispatch = useDispatch();

  const schema = yup.object().shape({
    sub: yup.string().required('Subcategory is required')
  });

  const {
    control,
    handleSubmit,
    formState: { isValid },
    setValue,
    reset
  } = useForm({
    resolver: yupResolver(schema),
    mode: 'all',
    defaultValues: {
      sub: ''
    }
  });

  const resetState = () => {
    setValue('title', '');
    closeModal();
  };

  const submit = async (values) => {
    const data = { ...values, id };
    console.log(data);
    const res = await dispatch(addSubCategory(data));
    if (res.payload) {
      resetState();
    }
  };

  useEffect(() => {
    reset();
  }, [open]);

  return (
    <Modal onClose={closeModal} open={open}>
      <form
        onSubmit={handleSubmit(submit)}
        className={`${style.content} ${!isLight && style.dark}`}>
        <div className={style.header}>
          <h3>Add subcategory</h3>
        </div>

        <div>
          <p style={{ marginBottom: '10px' }}>
            Category: <strong>{title}</strong>
          </p>

          <Controller
            render={({ field: { value, onChange }, fieldState: { error } }) => (
              <CustomizeInput
                size="lg"
                label="Subcategory"
                className={style.input}
                onChange={onChange}
                error={error}
                value={value}
                fullWidth
              />
            )}
            name="sub"
            control={control}
          />
        </div>

        <div className={style.footer}>
          <CustomizeButton
            type="button"
            variant="contained"
            onClick={closeModal}
            title="Cancel"
            size="small"
            color="neutral"
          />
          <CustomizeButton
            type="submit"
            variant="contained"
            title="Add"
            size="small"
            color="primary"
            loading={loading}
            disabled={loading}
          />
        </div>
      </form>
    </Modal>
  );
};

AddSubCategory.propTypes = {
  open: PropTypes.bool,
  closeModal: PropTypes.func,
  category: PropTypes.object
};

export default AddSubCategory;
