import React, { useEffect } from 'react';
import PropTypes from 'prop-types';
import IconButton from '@mui/material/IconButton';
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableHead from '@mui/material/TableHead';
import TableRow from '@mui/material/TableRow';
import { useDispatch, useSelector } from 'react-redux';
import { DeleteOutline, DetailsOutlined, MoreHorizOutlined } from '@mui/icons-material';
import { Chip, Menu } from '@mui/material';

import CustomizePagination from '../CustomizePagination';
import { useNavigate, useSearchParams } from 'react-router-dom';
import style from './style.module.scss';
import { getUsers } from '../../redux/slices/user/api';
import Loading from '../Loading';

const getStatus = (isActive) => {
  return isActive > 0 ? (
    <Chip
      sx={{ fontSize: '10px' }}
      size="small"
      label="Active"
      variant="outlined"
      color="success"
    />
  ) : (
    <Chip
      sx={{ fontSize: '10px' }}
      size="small"
      label="Inactive"
      variant="outlined"
      color="error"
    />
  );
};

function Row({ row }) {
  const { avatar, email, firstName, lastName, gender, isActive } = row;
  const [open, setOpen] = React.useState(false);
  const dispatch = useDispatch();
  const { isLight } = useSelector((state) => state.themeReducer);

  const navigate = useNavigate();

  const menuPopover = () => {
    const [anchorEl, setAnchorEl] = React.useState(null);
    const openMenu = Boolean(anchorEl);
    const handleClick = (event) => {
      setAnchorEl(event.currentTarget);
    };
    const handleClose = () => {
      setAnchorEl(null);
    };
    const onDelete = () => {};

    const onDetail = () => {
      navigate(`/users/${id}`);
    };

    return (
      <>
        <IconButton
          id="basic-button"
          aria-controls={openMenu ? 'basic-menu' : undefined}
          aria-haspopup="true"
          aria-expanded={openMenu ? 'true' : undefined}
          onClick={handleClick}>
          <MoreHorizOutlined />
        </IconButton>
        <Menu
          id="basic-menu"
          anchorEl={anchorEl}
          open={openMenu}
          onClose={handleClose}
          MenuListProps={{
            'aria-labelledby': 'basic-button'
          }}>
          <div className={`${style.menu} ${!isLight && style.dark}`}>
            <div onClick={onDetail} className={style.menuItem}>
              <span>Detail</span>
              <DetailsOutlined />
            </div>
            <div className={style.menuItem} onClick={onDelete}>
              <span>Delete</span>
              <DeleteOutline />
            </div>
          </div>
        </Menu>
      </>
    );
  };

  return (
    <>
      <TableRow sx={{ '& > *': { borderBottom: 'unset' } }}>
        <TableCell className={style.cell}>
          {avatar ? <img className={style.img} src={avatar} /> : 'No avatar'}
        </TableCell>
        <TableCell className={style.cell} component="th" scope="row">
          {email}
        </TableCell>
        <TableCell className={style.cell} align="right">
          {firstName}
        </TableCell>
        <TableCell className={style.cell} align="right">
          {lastName}
        </TableCell>
        <TableCell className={style.cell} align="right">
          {gender}
        </TableCell>
        <TableCell className={style.cell} align="right">
          {getStatus(isActive)}
        </TableCell>
        <TableCell className={style.cell} align="right">
          {/* {menuPopover(isActive)} */}
        </TableCell>
      </TableRow>
    </>
  );
}

Row.propTypes = {
  row: PropTypes.object
};

export default function UserList() {
  const { isLight } = useSelector((state) => state.themeReducer);

  const { userData, loading } = useSelector((state) => state.userReducer);

  const navigate = useNavigate();
  const dispatch = useDispatch();

  const [searchParams] = useSearchParams();

  const currentPage = searchParams.get('p') ? +searchParams.get('p') : 1;

  const onPageChange = (event, value) => {
    navigate(`/users?p=${value}`);
  };

  useEffect(() => {
    let stop = false;
    if (!stop) dispatch(getUsers({ p: currentPage }));
    return () => (stop = true);
  }, [currentPage]);

  const renderPagination = () => {
    const { pageCount } = userData;

    return (
      <CustomizePagination
        onChange={onPageChange}
        disabled={false}
        count={pageCount}
        page={currentPage}
        // a
      />
    );
  };

  return (
    <>
      <Loading loading={loading} />
      <TableContainer className={`${style.container} ${!isLight && style.dark}`}>
        <Table aria-label="collapsible table">
          <TableHead>
            <TableRow>
              <TableCell className={style.cellHead}>Avatar</TableCell>
              <TableCell className={style.cellHead}>Email</TableCell>
              <TableCell className={style.cellHead} align="right">
                Firstname
              </TableCell>
              <TableCell className={style.cellHead} align="right">
                Lastname
              </TableCell>
              <TableCell className={style.cellHead} align="right">
                Gender
              </TableCell>
              <TableCell className={style.cellHead} align="right">
                Status
              </TableCell>
              <TableCell className={style.cellHead} align="right">
                Action
              </TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {userData?.clients?.map((row) => (
              <Row key={row._id} row={row} />
            ))}
          </TableBody>
        </Table>
        <div style={{ padding: '10px' }}>{userData?.clients && renderPagination()}</div>
      </TableContainer>
    </>
  );
}
