import React from 'react';
import CustomizeButton from '../CustomizeButton';
import style from './style.module.scss';

const AddComplexProductFooter = () => {
  return (
    <div className={style.container}>
      <CustomizeButton
        title="Previous"
        type="button"
        color="neutral"
        variant="contained"
        size="medium"
      />
      <CustomizeButton
        title="Next"
        type="button"
        color="primary"
        variant="contained"
        size="medium"
      />
    </div>
  );
};

export default AddComplexProductFooter;
