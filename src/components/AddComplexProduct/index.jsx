import { Grid, IconButton, Modal } from '@mui/material';
import React, { useEffect, useState } from 'react';
import PropTypes from 'prop-types';
import style from './style.module.scss';
import { useDispatch, useSelector } from 'react-redux';
import { yupResolver } from '@hookform/resolvers/yup';
import * as yup from 'yup';
import { Controller, useForm } from 'react-hook-form';
import CustomizeButton from '../CustomizeButton';
import CustomizeInput from '../CustomizeInput';
import { CloseOutlined, UploadOutlined } from '@mui/icons-material';
import CustomizeSelect from '../CustomizeSelect';
import { fileListToBase64 } from '../../utils/toBase64';
import { nextStep, saveComplexData } from '../../redux/slices/product';
import { useNavigate } from 'react-router-dom';
import { addProduct } from '../../redux/slices/product/api';
import { addAlert } from '../../redux/slices/alert';

const AttrItem = ({ attr, idx, onChangeKey, onChangeValues }) => {
  const { key, initialValues, values } = attr;

  const { isLight } = useSelector((state) => state.themeReducer);

  return (
    <div className={`${style.attrBlock} ${!isLight && style.dark}`}>
      <div className={style.blockItem}>
        <CustomizeInput
          fullWidth
          label="Key"
          value={key}
          onChange={onChangeKey}
          size="md"
          placeholder="Ex:Color"
        />
        <CustomizeInput
          fullWidth
          label="Values (Separate the value with coma (dấu phẩy))"
          value={initialValues}
          onChange={onChangeValues}
          size="md"
          placeholder="Ex: red,blue,green"
        />
      </div>
      {key && (
        <p className={style.values}>
          {key}:{' '}
          {values.map((val, i) => (
            <span className={`${style.val} ${!isLight && style.dark}`} key={i}>
              {val}
            </span>
          ))}
        </p>
      )}
    </div>
  );
};

const AddComplexProduct = () => {
  const { isLight } = useSelector((state) => state.themeReducer);
  const { loading, complexData } = useSelector((state) => state.productReducer);
  const { categories } = useSelector((state) => state.categoryReducer);

  const [categoryList, setCategoryList] = useState([]);
  const [subcategoryList, setSubCategoryList] = useState([]);

  const [displayedImages, setDisplayedImages] = useState([]);

  const [attributes, setAttributes] = useState([
    {
      key: '',
      initialValues: '',
      hasImage: false,
      values: []
    }
  ]);

  const [variantImg, setVariantImg] = useState('');

  const dispatch = useDispatch();
  const navigate = useNavigate();

  const schema = yup.object().shape({
    title: yup.string().required(),
    price: yup
      .number()
      .required()
      .nullable(true)
      .transform((_, val) => (val ? Number(val) : null)),
    category: yup.string().required(),
    subCategory: yup.string().required(),
    brand: yup.string(),

    description: yup.string().required(),
    images: yup.mixed().test('images', 'Images are required', (values) => {
      return values?.length > 0;
    })
  });

  const { control, handleSubmit, setValue, watch } = useForm({
    resolver: yupResolver(schema),
    mode: 'all',
    defaultValues: {
      title: '',
      category: '',
      subCategory: '',
      description: '',
      brand: '',
      price: '',
      productType: 'complex'
    }
  });

  const watchCategory = watch('category');

  const watchImages = watch('images');

  const hasAttributes = () => {
    let result = false;
    if (attributes.every((el) => el.key && el.values.length > 0)) {
      result = true;
      return result;
    }
    dispatch(
      addAlert({
        title: 'Error',
        content: 'Attribute must have a key and values',
        severity: 'error'
      })
    );
    return result;
  };

  const hasTheSameKey = () => {
    let result = false;
    const keys = attributes.map((el) => el.key);
    if (keys[0] === keys[1]) {
      result = true;
      dispatch(
        addAlert({
          title: 'Error',
          content: 'Attribute can not have the same key ',
          severity: 'error'
        })
      );
    }
    return result;
  };

  const doNotHaveSimilarValue = (arr) => {
    const formatedArr = arr.filter((el, i) => arr.indexOf(el) === i);
    const result = arr.length === formatedArr.length;
    console.log(result);
    return result;
  };

  const allAttrDoNotHaveSimilarValue = () => {
    let result = true;
    const values = attributes.map((el) => el.values);
    result = values.every((arr) => doNotHaveSimilarValue(arr));
    if (result) {
      return result;
    }
    dispatch(
      addAlert({
        title: 'Error',
        content: 'Attribute can not have the same value ',
        severity: 'error'
      })
    );
    return result;
  };

  const submit = async (data) => {
    if (!hasAttributes()) return '';
    if (hasTheSameKey()) return '';
    if (!allAttrDoNotHaveSimilarValue()) return '';

    const newAttrs = [...attributes];
    newAttrs.forEach((el) => {
      el.hasImage = el.key === variantImg ? true : false;
      delete el.initialValues;
    });
    const mainAttributes = JSON.stringify(newAttrs);

    const formData = new FormData();

    const keys = Object.keys(data);

    for (let key of keys) {
      if (key !== 'images') formData.append(key, data[key]);
    }
    for (let file of data.images) {
      formData.append('images', file);
    }
    formData.append('attributes', mainAttributes);

    const res = await dispatch(addProduct(formData));

    if (res.payload) navigate('/products');
  };

  // Map to get category title for select
  useEffect(() => {
    if (categories?.length > 0) setCategoryList(categories.map((el) => el.title));
  }, [categories]);

  //   Get subcategory list for select
  useEffect(() => {
    if (categories.length > 0 && watchCategory) {
      const currentSubs = categories.find((el) => el.title === watchCategory).subs;
      setSubCategoryList(currentSubs);
    } else {
      setSubCategoryList([]);
    }
  }, [watchCategory, categories]);

  useEffect(() => {
    if (!complexData) {
      setValue('subCategory', '');
    } else {
      if (complexData.category !== watchCategory) setValue('subCategory', '');
    }
  }, [watchCategory, complexData]);

  //   Set value when edit
  useEffect(() => {
    if (complexData) {
      setValue('title', complexData.title);
      setValue('category', complexData.category);

      setValue('subCategory', complexData.subCategory);

      setValue('description', complexData.description);

      setValue('brand', complexData.brand);

      setValue('images', complexData.images);
    }
  }, [complexData]);

  useEffect(() => {
    if (watchImages?.length > 0) {
      fileListToBase64(watchImages).then((values) => {
        setDisplayedImages(values);
      });
    } else {
      setDisplayedImages([]);
    }
  }, [watchImages]);

  const handleChangeKey = (val, idx) => {
    const newAttrs = [...attributes];
    newAttrs[idx]['key'] = val.toLowerCase();
    setAttributes(newAttrs);
  };

  const handleChangeValues = (val, idx) => {
    const newAttrs = [...attributes];
    newAttrs[idx].initialValues = val;
    const formatValues = val
      .split(',')
      .map((el) => el.replace(/^\s+|\s+$/g, ''))
      .filter((el) => el);
    newAttrs[idx].values = formatValues;
    setAttributes(newAttrs);
  };

  const addNewAttr = () => {
    if (attributes.length < 2)
      setAttributes([...attributes, { key: '', values: [], initialValues: '', hasImage: false }]);
  };

  return (
    <>
      <form
        onSubmit={handleSubmit(submit)}
        className={`${style.content} ${!isLight && style.dark}`}>
        <div className={style.block}>
          <h3>General info</h3>
          <Grid container spacing={3}>
            <Grid item xs={12} md={6}>
              <Controller
                render={({ field: { value, onChange }, fieldState: { error } }) => (
                  <CustomizeInput
                    fullWidth
                    size="lg"
                    label="Title"
                    className={style.input}
                    onChange={onChange}
                    error={error}
                    value={value}
                    type="text"
                    placeholder="Ex: Basic tee"
                  />
                )}
                name="title"
                control={control}
              />
            </Grid>
            <Grid item xs={12} md={6}>
              <Controller
                render={({ field: { value, onChange }, fieldState: { error } }) => (
                  <CustomizeInput
                    fullWidth
                    size="lg"
                    label="Price"
                    className={style.input}
                    onChange={onChange}
                    error={error}
                    value={value}
                    type="number"
                    placeholder="Ex: 30"
                  />
                )}
                name="price"
                control={control}
              />
            </Grid>

            <Grid xs={12} item md={6}>
              <Controller
                render={({ field: { value, onChange }, fieldState: { error } }) => (
                  <CustomizeSelect
                    label="Category"
                    className={style.input}
                    onChange={onChange}
                    error={error}
                    value={value}
                    options={categoryList}
                    size="lg"
                    full
                  />
                )}
                name="category"
                control={control}
              />
            </Grid>

            <Grid xs={12} item md={6}>
              <Controller
                render={({ field: { value, onChange }, fieldState: { error } }) => (
                  <CustomizeSelect
                    size="lg"
                    label="Subcategory"
                    className={style.input}
                    onChange={onChange}
                    error={error}
                    value={value}
                    options={subcategoryList}
                    full
                  />
                )}
                name="subCategory"
                control={control}
              />
            </Grid>
            <Grid item xs={12} md={6}>
              <Controller
                render={({ field: { value, onChange }, fieldState: { error } }) => (
                  <CustomizeInput
                    fullWidth
                    size="lg"
                    label="Brand (optional)"
                    className={style.input}
                    onChange={onChange}
                    error={error}
                    value={value}
                    type="text"
                    placeholder="Ex: Amazon"
                  />
                )}
                name="brand"
                control={control}
              />
            </Grid>
            <Grid item xs={12}>
              <Controller
                render={({ field: { value, onChange }, fieldState: { error } }) => (
                  <CustomizeInput
                    fullWidth
                    multiline={true}
                    size="lg"
                    label="Description"
                    className={style.input}
                    onChange={onChange}
                    error={error}
                    value={value}
                    rows={4}
                    placeholder="Ex:This product is..."
                  />
                )}
                name="description"
                control={control}
              />
            </Grid>
          </Grid>
        </div>

        <div className={style.block}>
          <h3>Images</h3>
          <label htmlFor="contained-button-file">
            <Controller
              defaultValue=""
              render={({ field: { onChange, value }, fieldState: { error } }) => {
                return (
                  <>
                    <input
                      style={{ display: 'none' }}
                      accept="image/*"
                      type="file"
                      value={value.files}
                      onChange={(e) => {
                        onChange(e.target.files);
                      }}
                      placeholder="Images"
                      id="contained-button-file"
                      multiple
                    />

                    <div className={style.uploadBlock}>
                      <div className={`${style.upload} ${!isLight && style.dark}`}>
                        <p>Upload images</p>
                        <UploadOutlined />
                      </div>
                      <small className={style.error}>{error && error.message}</small>
                    </div>
                  </>
                );
              }}
              name="images"
              control={control}
            />
          </label>
          {displayedImages.length > 0 ? (
            <div className={style.images}>
              {displayedImages.map((img, i) => (
                <img key={i} src={img} />
              ))}
            </div>
          ) : (
            ''
          )}
        </div>

        <div className={style.block}>
          <h3>Attributes</h3>

          <div className={style.attr}>
            {attributes.map((attr, idx) => (
              <AttrItem
                key={idx}
                onChangeKey={(e) => handleChangeKey(e.target.value, idx)}
                onChangeValues={(e) => handleChangeValues(e.target.value, idx)}
                attr={attr}
                idx={idx}
              />
            ))}

            <CustomizeButton
              onClick={addNewAttr}
              title="More attribute"
              variant="outlined"
              color="secondary"
              type="button"
              size="small"
            />
          </div>
        </div>

        <div className={style.block}>
          <h3>Variant image</h3>

          <div className={style.attr}>
            <CustomizeInput
              label="Key"
              fullWidth
              size="md"
              value={variantImg}
              onChange={(e) => setVariantImg(e.target.value)}
            />
            <p className={style.noti}>Enter the key of one attribute that has images.</p>
            <p className={style.noti}>
              If this field is blank or is not the same as any attribute key then all variants will
              have the same images.
            </p>
          </div>
        </div>

        <div className={style.footer}>
          <CustomizeButton
            title="Save"
            type="submit"
            color="primary"
            variant="contained"
            size="lg"
          />
        </div>
      </form>
    </>
  );
};

export default AddComplexProduct;
