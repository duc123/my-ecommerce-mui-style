import { FormControl, FormHelperText, InputLabel, OutlinedInput } from '@mui/material';
import React from 'react';
import PropTypes from 'prop-types';
import style from './style.module.scss';
const AuthInput = ({ placeholder, error, value, onChange, endAdornment, type }) => {
  return (
    <FormControl error={error ? true : false} className={style.control} variant="outlined">
      <InputLabel htmlFor="outlined-adornment-password">{placeholder}</InputLabel>
      <OutlinedInput
        className={style.input}
        size="large"
        type={type}
        value={value}
        onChange={onChange}
        label={placeholder}
        endAdornment={endAdornment}
      />
      {error && <FormHelperText>{error.message}</FormHelperText>}
    </FormControl>
  );
};

AuthInput.propTypes = {
  placeholder: PropTypes.string,
  error: PropTypes.object,
  value: PropTypes.string,
  onChange: PropTypes.func,
  endAdornment: PropTypes.element,
  type: PropTypes.string
};

export default AuthInput;
