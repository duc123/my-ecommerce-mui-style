import { ArrowRightAltOutlined, StackedLineChartOutlined } from '@mui/icons-material';
import React, { useState } from 'react';
import { useSelector } from 'react-redux';
import { Link } from 'react-router-dom';
import Crypto from '../../components/Crypto';
import CustomizeButton from '../../components/CustomizeButton';
import style from './style.module.scss';

const Component = ({ item }) => {
  const { isLight } = useSelector((state) => state.themeReducer);

  const { title, data, end, url } = item;
  return (
    <div className={`${style.component} ${style.staItem} ${!isLight ? style.dark : ''}`}>
      <div className={style.block}>
        <div>
          <p>{title}</p>
          <h3>
            {data} {end && end}
          </h3>
        </div>
        <span className={style.icon}>
          <StackedLineChartOutlined />
        </span>
      </div>
      <Link to={`/${url}`}>
        <CustomizeButton
          title="View"
          size="xs"
          color="neutral"
          variant="contained"
          endIcon={<ArrowRightAltOutlined sx={{ fontSize: '15px' }} />}
        />
      </Link>
    </div>
  );
};

const Home = () => {
  const { isLight } = useSelector((state) => state.themeReducer);

  const { numProducts, numClients, revenue, numBuyers } = useSelector(
    (state) => state.statisticReducer
  );

  const list = [
    {
      title: 'Products',
      data: numProducts,
      url: 'products'
    },
    {
      title: 'Clients',
      data: numClients,
      url: 'users'
    },
    {
      title: 'Revenue',
      data: revenue,
      end: '$',
      url: 'orders'
    },
    {
      title: 'Buyers',
      data: numBuyers,
      url: 'orders'
    }
  ];

  return (
    <div className={style.container}>
      <div className={style.statistics}>
        {list.map((item, i) => (
          <Component key={i} item={item} />
        ))}
      </div>
      <div className={`${style.component} ${!isLight ? style.dark : ''}`}>
        <Crypto />
      </div>
    </div>
  );
};

export default Home;
