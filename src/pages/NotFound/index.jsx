import React from 'react';
import { Link, useNavigate } from 'react-router-dom';
import CustomizeButton from '../../components/CustomizeButton';
import style from './style.module.scss';

const NotFound = () => {
  const navigate = useNavigate();

  const handleBack = () => navigate(-1);

  return (
    <div className={style.container}>
      <div style={{ textAlign: 'center' }}>
        <h2>404 Not found</h2>
        <div className={style.flex}>
          <CustomizeButton title="Go back" onClick={handleBack} size="large" variant="outlined" />

          <Link to="/">
            <CustomizeButton title="To home" size="large" variant="contained" />
          </Link>
        </div>
      </div>
    </div>
  );
};

export default NotFound;
