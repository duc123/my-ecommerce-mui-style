import { AddOutlined } from '@mui/icons-material';
import React from 'react';
import { useSelector } from 'react-redux';
import { Link } from 'react-router-dom';
import CustomizeButton from '../../components/CustomizeButton';
import OrderList from '../../components/OrderList';
import style from './style.module.scss';

const Orders = () => {
  const { isLight } = useSelector((state) => state.themeReducer);

  return (
    <div>
      <div className={style.flex}>
        <h1>Orders</h1>
      </div>
      <div className={`${style.container} ${!isLight && style.dark}`}>
        {/* <SearchProducts /> */}
        <OrderList />
      </div>
    </div>
  );
};

export default Orders;
