import React from 'react';
import { useSelector } from 'react-redux';
import { Link } from 'react-router-dom';
import UserList from '../../components/UserList';
import style from './style.module.scss';

const Users = () => {
  const { isLight } = useSelector((state) => state.themeReducer);

  return (
    <div>
      <div className={style.flex}>
        <h1>Users</h1>
      </div>
      <div className={`${style.container} ${!isLight && style.dark}`}>
        {/* <SearchUser /> */}
        <UserList />
      </div>
    </div>
  );
};

export default Users;
