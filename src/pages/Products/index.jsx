import { AddOutlined } from '@mui/icons-material';
import React from 'react';
import { useSelector } from 'react-redux';
import { Link } from 'react-router-dom';
import CustomizeButton from '../../components/CustomizeButton';
import ProductList from '../../components/ProductList';
import SearchProducts from '../../components/SearchProducts';
import style from './style.module.scss';

const Products = () => {
  const { isLight } = useSelector((state) => state.themeReducer);

  return (
    <div>
      <div className={style.flex}>
        <h1>Products</h1>
        <Link to={`/addProduct`}>
          <CustomizeButton
            title="New product"
            endIcon={<AddOutlined />}
            variant="contained"
            size="large"
            type="button"
          />
        </Link>
      </div>
      <div className={`${style.container} ${!isLight && style.dark}`}>
        <SearchProducts />
        <ProductList />
      </div>
    </div>
  );
};

export default Products;
