import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useParams } from 'react-router-dom';
import {
  getSingleOrder,
  markDelivered,
  markPaid,
  markUnDelivered,
  markUnPaid
} from '../../api/order';
import CustomizeButton from '../../components/CustomizeButton';
import Loading from '../../components/Loading';
import { addAlert } from '../../redux/slices/alert';
import { formatCurrency } from '../../utils/currency';
import style from './style.module.scss';

const Order = ({ order, onPaid, onUnPaid, onDelivered, onUnDelivered }) => {
  const renderAttr = () => {
    const keys = attributes.map((el) => el.key);
    return keys.map((k) => (
      <div key={k} className={style.box}>
        <p>{k}:</p>
        <p>{variant[k]}</p>
      </div>
    ));
  };

  const { isLight } = useSelector((state) => state.themeReducer);

  const {
    shipping,
    title,
    price,
    quantity,
    image,
    attributes,
    variant,
    paymentStatus,
    deliveryStatus
  } = order;

  const { city, district, ward, detail, phone } = shipping;

  return (
    <div className={style.container}>
      <div className={style.block}>
        <img src={image} />
      </div>
      <div className={`${isLight ? 'componentBg' : 'componentBgDark'} ${style.block}`}>
        <h3>Details</h3>
        <div className={style.box}>
          <p>Title:</p>
          <p>{title}</p>
        </div>
        <div className={style.box}>
          <p>Price:</p>
          <p>{formatCurrency(price)}$</p>
        </div>
        <div className={style.box}>
          <p>Quantity:</p>
          <p>{quantity}</p>
        </div>
        {attributes?.length > 0 && renderAttr()}
      </div>
      <div className={`${isLight ? 'componentBg' : 'componentBgDark'} ${style.block}`}>
        <h3>Shipping</h3>
        <div className={style.box}>
          <p>City:</p>
          <p>{city}</p>
        </div>
        <div className={style.box}>
          <p>District:</p>
          <p>{district}</p>
        </div>
        <div className={style.box}>
          <p>Ward:</p>
          <p>{ward}</p>
        </div>
        <div className={style.box}>
          <p>Address:</p>
          <p>{detail}</p>
        </div>
        <div className={style.box}>
          <p>Phone number:</p>
          <p>{phone}</p>
        </div>
      </div>
      <div className={`${isLight ? 'componentBg' : 'componentBgDark'} ${style.block}`}>
        <h3>Payment status</h3>

        <div className={style.actions}>
          {paymentStatus ? (
            <CustomizeButton
              onClick={onUnPaid}
              title="Mark as Undone"
              color="primary"
              variant="outlined"
              size="large"
            />
          ) : (
            <CustomizeButton
              onClick={onPaid}
              title="Mark as Done"
              color="primary"
              variant="contained"
              size="large"
            />
          )}
        </div>
      </div>
      <div className={`${isLight ? 'componentBg' : 'componentBgDark'} ${style.block}`}>
        <h3>Delivery status</h3>

        <div className={style.actions}>
          {deliveryStatus ? (
            <CustomizeButton
              onClick={onUnDelivered}
              title="Mark as Undone"
              color="primary"
              variant="outlined"
              size="large"
            />
          ) : (
            <CustomizeButton
              onClick={onDelivered}
              title="Mark as Done"
              color="primary"
              variant="contained"
              size="large"
            />
          )}
        </div>
      </div>
    </div>
  );
};

const SingleOrder = () => {
  const { id } = useParams();

  const dispatch = useDispatch();

  const [order, setOrder] = useState(null);

  const [loading, setLoading] = useState(false);

  const getOrder = async (id) => {
    setLoading(true);
    try {
      const res = await getSingleOrder(id);
      setOrder(res);
    } catch (err) {
      dispatch(addAlert({ severity: 'error', title: 'Error', content: err }));
    }
    setLoading(false);
  };

  useEffect(() => {
    getOrder(id);
  }, [id]);

  const onPaid = async () => {
    setLoading(true);
    try {
      const order = await markPaid(id);
      setOrder(order);
    } catch (err) {
      dispatch(addAlert({ severity: 'error', title: 'Error', content: err }));
    }
    setLoading(false);
  };

  const onUnPaid = async () => {
    setLoading(true);

    try {
      const order = await markUnPaid(id);
      setOrder(order);
    } catch (err) {
      dispatch(addAlert({ severity: 'error', title: 'Error', content: err }));
    }
    setLoading(false);
  };

  const onDelivered = async () => {
    setLoading(true);

    try {
      const order = await markDelivered(id);
      setOrder(order);
    } catch (err) {
      dispatch(addAlert({ severity: 'error', title: 'Error', content: err }));
    }
    setLoading(false);
  };

  const onUnDelivered = async () => {
    setLoading(true);

    try {
      const order = await markUnDelivered(id);
      setOrder(order);
    } catch (err) {
      dispatch(addAlert({ severity: 'error', title: 'Error', content: err }));
    }
    setLoading(false);
  };

  return (
    <div>
      <Loading loading={loading} />
      {order && (
        <Order
          onPaid={onPaid}
          onUnPaid={onUnPaid}
          onDelivered={onDelivered}
          onUnDelivered={onUnDelivered}
          order={order}
        />
      )}
    </div>
  );
};

export default SingleOrder;
