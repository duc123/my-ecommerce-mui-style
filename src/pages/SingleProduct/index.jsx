import { Table, TableBody, TableCell, TableContainer, TableHead, TableRow } from '@mui/material';
import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useParams } from 'react-router-dom';
import CustomizeButton from '../../components/CustomizeButton';
import PriceCellElement from '../../components/PriceCellElement';
import StockCellElement from '../../components/StockCellElement';
import VariantImages from '../../components/VariantImages';
import axiosClient from '../../lib/axiosClient';
import { addAlert } from '../../redux/slices/alert';
import style from './style.module.scss';

const ProductNav = ({ navs, changeNav, currentNav }) => {
  const handleClick = (nav) => () => changeNav(nav);

  return (
    <div className={style.navs}>
      {navs.map((nav) => (
        <p
          className={`${style.nav} ${nav === currentNav && style.active}`}
          onClick={handleClick(nav)}
          key={nav}>
          {nav}
        </p>
      ))}
    </div>
  );
};

const GeneralInfo = ({ product }) => {
  const { title, price, images, category, subCategory, productType, _id: id, attributes } = product;
  return (
    <div className={style.centerDiv}>
      <div className={style.block}>
        <span>Id:</span>
        <p>{id}</p>
      </div>
      <div className={style.block}>
        <span>Type:</span>

        <p>{productType}</p>
      </div>
      <div className={style.block}>
        <span>Title:</span>
        <p>{title}</p>
      </div>
      <div className={style.block}>
        <span>Price:</span>
        <p>{price}$</p>
      </div>
      <div className={style.block}>
        <span>Category:</span>
        <p>{category}</p>
      </div>
      <div className={style.block}>
        <span>Subcategory:</span>
        <p>{subCategory}</p>
      </div>
      <div className={style.block}>
        <span>Attributes:</span>
        <p className={style.values}>
          {attributes.length > 0
            ? attributes.map((attr) => (
                <span className={style.val} key={attr.key}>
                  {attr.key}
                </span>
              ))
            : 'No attribute'}
        </p>
      </div>
      {attributes.length > 0 &&
        attributes.map((attr) => (
          <div key={attr.key} className={style.block}>
            <span>{attr.key}</span>
            <p className={style.values}>
              {attr.values.map((val) => (
                <span key={val} className={style.val}>
                  {val}
                </span>
              ))}
            </p>
          </div>
        ))}
      <div className={style.images}>
        {images.map((img, idx) => (
          <img className={style.img} key={idx} src={img} />
        ))}
      </div>
    </div>
  );
};

const Variants = ({ product, setProduct }) => {
  const { variants, attributes, images } = product;
  const keys = attributes.map((el) => el.key);

  return (
    <TableContainer>
      <Table size="small" sx={{ minWidth: 650 }} aria-label="simple table">
        <TableHead>
          <TableRow>
            <TableCell>Price</TableCell>
            <TableCell>Stock</TableCell>
            {keys.map((k) => (
              <TableCell key={k} align="right">
                <span className={style.key}>{k}</span>
              </TableCell>
            ))}
            <TableCell align="right">Images</TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          {variants.map((variant, idx) => {
            const varImages = variant.images.length > 0 ? variant.images : images;
            return (
              <TableRow key={idx} sx={{ '&:last-child td, &:last-child th': { border: 0 } }}>
                <TableCell component="th" scope="row">
                  <PriceCellElement variant={variant} product={product} setProduct={setProduct} />
                </TableCell>
                <TableCell align="right">
                  <StockCellElement variant={variant} product={product} setProduct={setProduct} />
                </TableCell>
                {keys.map((k) => (
                  <TableCell key={k} align="right">
                    {variant[k]}
                  </TableCell>
                ))}

                <TableCell align="right">
                  <div className={style.varImages}>
                    {varImages.map((img, i) => (
                      <img src={img} key={i} />
                    ))}
                  </div>
                </TableCell>
              </TableRow>
            );
          })}
        </TableBody>
      </Table>
    </TableContainer>
  );
};

const SingleProduct = () => {
  const params = useParams();

  const { isLight } = useSelector((state) => state.themeReducer);

  const [product, setProduct] = useState(null);
  const [navs, setNavs] = useState(['General information']);
  const [currentNav, setCurrentNav] = useState(navs[0]);

  const changeNav = (nav) => setCurrentNav(nav);

  const { id } = params;

  const dispatch = useDispatch();

  const fetchProduct = async () => {
    try {
      const res = await axiosClient({
        url: `/products/getSingle/${id}`
      });
      const { data } = res;
      setProduct(data.product);
    } catch (err) {
      dispatch(addAlert({ severity: 'error', title: 'Error', content: err.response.data.error }));
    }
  };

  useEffect(() => {
    if (id) fetchProduct();
  }, [id]);

  useEffect(() => {
    if (product?.productType === 'complex')
      setNavs(['General information', 'Variants', 'Variant images']);
  }, [product]);

  const renderBlock = () => {
    switch (currentNav) {
      case 'Variants':
        return <Variants product={product} setProduct={setProduct} />;
      case 'Variant images':
        return <VariantImages product={product} setProduct={setProduct} />;
      default:
        return <GeneralInfo product={product} />;
    }
  };

  return (
    <div className={`${style.container} ${!isLight && style.dark}`}>
      {product && (
        <>
          <ProductNav navs={navs} changeNav={changeNav} currentNav={currentNav} />
          {renderBlock()}
        </>
      )}
    </div>
  );
};

export default SingleProduct;
