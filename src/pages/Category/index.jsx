import { AddOutlined } from '@mui/icons-material';
import { Button, Grid } from '@mui/material';
import React, { useState } from 'react';
import { useSelector } from 'react-redux';
import AddCategory from '../../components/AddCategory';
import CategoryItem from '../../components/CategoryItem';
import style from './style.module.scss';

const Category = () => {
  const { categories } = useSelector((state) => state.categoryReducer);

  const [open, setOpen] = useState(false);

  const renderList = () => {
    return (
      <Grid spacing={{ xs: 5, md: 6, lg: 5 }} container>
        {categories.map((item) => (
          <Grid key={item._id} item xs={6} md={4} lg={3}>
            <CategoryItem item={item} />
          </Grid>
        ))}
      </Grid>
    );
  };

  const openModal = () => setOpen(true);

  const closeModal = () => setOpen(false);

  return (
    <div>
      <AddCategory open={open} closeModal={closeModal} />
      <div className={style.flex}>
        <h1 className={style.title}>Category</h1>
        <Button onClick={openModal} variant="contained" endIcon={<AddOutlined />}>
          Add category
        </Button>
      </div>
      {categories?.length > 0 ? renderList() : ''}
    </div>
  );
};

export default Category;
