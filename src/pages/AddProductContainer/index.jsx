import React from 'react';
import { useSelector } from 'react-redux';
import { Outlet } from 'react-router-dom';
import style from './style.module.scss';

const AddProductContainer = () => {
  const { isLight } = useSelector((state) => state.themeReducer);

  return (
    <div className={`${style.container} ${!isLight && style.dark}`}>
      <Outlet />
    </div>
  );
};

export default AddProductContainer;
