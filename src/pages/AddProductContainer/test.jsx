// import { Grid, IconButton, Modal } from '@mui/material';
// import React, { useEffect, useState } from 'react';
// import PropTypes from 'prop-types';
// import style from './style.module.scss';
// import { useDispatch, useSelector } from 'react-redux';
// import { yupResolver } from '@hookform/resolvers/yup';
// import * as yup from 'yup';
// import { Controller, useForm } from 'react-hook-form';
// import CustomizeButton from '../../components/CustomizeButton';
// import CustomizeInput from '../../components/CustomizeInput';
// import { CloseOutlined } from '@mui/icons-material';
// import CustomizeSelect from '../../components/CustomizeSelect';
// import { addProduct, editProduct } from '../../redux/slices/product/api';

// const AddProduct = ({ open, closeModal }) => {
//   const { isLight } = useSelector((state) => state.themeReducer);
//   const { loading, selectedProduct } = useSelector((state) => state.productReducer);
//   const { categories } = useSelector((state) => state.categoryReducer);
//   const { attributes: attributesList } = useSelector((state) => state.attributeReducer);

//   const [categoryList, setCategoryList] = useState([]);
//   const [subcategoryList, setSubCategoryList] = useState([]);

//   const dispatch = useDispatch();

//   const schema = yup.object().shape({
//     title: yup.string().required(),
//     category: yup.string().required(),
//     subCategory: yup.string().required(),
//     brand: yup.string(),
//     description: yup.string().required(),
//     attributes: yup.array()
//   });

//   const { control, handleSubmit, setValue, reset, watch } = useForm({
//     resolver: yupResolver(schema),
//     mode: 'all',
//     defaultValues: {
//       title: '',
//       category: '',
//       subCategory: '',
//       description: '',
//       attributes: [],
//       brand: ''
//     }
//   });

//   const watchCategory = watch('category');

//   const resetState = () => {
//     closeModal();
//   };

//   const submit = (data) => {
//     if (watch('attributes').length > 0) {
//       if (selectedProduct) {
//         onEdit(data);
//       } else {
//         onAdd(data);
//       }
//     }
//   };

//   const onAdd = async (data) => {
//     const res = await dispatch(addProduct(data));
//     if (res.payload) resetState();
//   };

//   const onEdit = async (para) => {
//     const data = { id: selectedProduct._id, data: para };
//     const res = await dispatch(editProduct(data));
//     if (res.payload) resetState();
//   };

//   useEffect(() => {
//     reset();
//   }, [open]);

//   useEffect(() => {
//     if (categories?.length > 0) setCategoryList(categories.map((el) => el.title));
//   }, [categories]);

//   useEffect(() => {
//     if (categories.length > 0 && watchCategory) {
//       const currentSubs = categories.find((el) => el.title === watchCategory).subs;
//       setSubCategoryList(currentSubs);
//     } else {
//       setSubCategoryList([]);
//     }
//   }, [watchCategory, categories]);

//   useEffect(() => {
//     if (!selectedProduct) {
//       setValue('subCategory', '');
//     } else {
//       if (selectedProduct.category !== watchCategory) setValue('subCategory', '');
//     }
//   }, [watchCategory, selectedProduct]);

//   useEffect(() => {
//     if (selectedProduct) {
//       setValue('title', selectedProduct.title);
//       setValue('category', selectedProduct.category);

//       setValue('subCategory', selectedProduct.subCategory);

//       setValue('description', selectedProduct.description);

//       setValue('brand', selectedProduct.brand);
//       setValue('attributes', selectedProduct.attributes);
//     }
//   }, [selectedProduct, open]);

//   return (
//     <Modal onClose={closeModal} open={open}>
//       <form
//         onSubmit={handleSubmit(submit)}
//         className={`${style.content} ${!isLight && style.dark}`}>
//         <div className={style.header}>
//           <h3>{selectedProduct ? 'Edit product' : 'Add Product'}</h3>
//           <IconButton onClick={closeModal}>
//             <CloseOutlined />
//           </IconButton>
//         </div>

//         <div className={style.body}>
//           <Grid container spacing={3}>
//             <Grid item xs={12} md={6}>
//               <Controller
//                 render={({ field: { value, onChange }, fieldState: { error } }) => (
//                   <CustomizeInput
//                     size="medium"
//                     label="Title"
//                     className={style.input}
//                     onChange={onChange}
//                     error={error}
//                     value={value}
//                   />
//                 )}
//                 name="title"
//                 control={control}
//               />
//             </Grid>
//             <Grid item xs={12} md={6}>
//               <Controller
//                 render={({ field: { value, onChange }, fieldState: { error } }) => (
//                   <CustomizeInput
//                     size="medium"
//                     label="Brand (optional)"
//                     className={style.input}
//                     onChange={onChange}
//                     error={error}
//                     value={value}
//                   />
//                 )}
//                 name="brand"
//                 control={control}
//               />
//             </Grid>
//             <Grid xs={12} item md={6}>
//               <Controller
//                 render={({ field: { value, onChange }, fieldState: { error } }) => (
//                   <CustomizeSelect
//                     size="medium"
//                     label="Category"
//                     className={style.input}
//                     onChange={onChange}
//                     error={error}
//                     value={value}
//                     options={categoryList}
//                   />
//                 )}
//                 name="category"
//                 control={control}
//               />
//             </Grid>

//             <Grid xs={12} item md={6}>
//               <Controller
//                 render={({ field: { value, onChange }, fieldState: { error } }) => (
//                   <CustomizeSelect
//                     size="medium"
//                     label="Subcategory"
//                     className={style.input}
//                     onChange={onChange}
//                     error={error}
//                     value={value}
//                     options={subcategoryList}
//                   />
//                 )}
//                 name="subCategory"
//                 control={control}
//               />
//             </Grid>
//             <Grid item xs={12}>
//               <Controller
//                 render={({ field: { value, onChange }, fieldState: { error } }) => (
//                   <CustomizeInput
//                     multiline={true}
//                     size="medium"
//                     label="Description"
//                     className={style.input}
//                     onChange={onChange}
//                     error={error}
//                     value={value}
//                     rows={4}
//                   />
//                 )}
//                 name="description"
//                 control={control}
//               />
//             </Grid>
//             <Grid xs={12} item md={6}>
//               <Controller
//                 render={({ field: { value, onChange }, fieldState: { error } }) => (
//                   <CustomizeSelect
//                     multiple={true}
//                     size="medium"
//                     label="Attributes"
//                     className={style.input}
//                     onChange={onChange}
//                     error={error}
//                     value={value}
//                     options={attributesList.map((el) => el.title)}
//                   />
//                 )}
//                 name="attributes"
//                 control={control}
//               />
//             </Grid>
//           </Grid>
//         </div>

//         <div className={style.footer}>
//           <CustomizeButton
//             type="button"
//             variant="contained"
//             onClick={closeModal}
//             title="Cancel"
//             size="medium"
//             color="neutral"
//           />
//           <CustomizeButton
//             type="submit"
//             variant="contained"
//             title="Save"
//             size="medium"
//             color="primary"
//             loading={loading}
//             disabled={loading}
//           />
//         </div>
//       </form>
//     </Modal>
//   );
// };

// AddProduct.propTypes = {
//   open: PropTypes.bool,
//   closeModal: PropTypes.func
// };

// export default AddProduct;
