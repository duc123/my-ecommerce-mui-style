import React, { useEffect, useState } from 'react';
import { Controller, useForm } from 'react-hook-form';
import style from './style.module.scss';
import { yupResolver } from '@hookform/resolvers/yup';
import * as yup from 'yup';
import AuthInput from '../../components/AuthInput';
import {
  DarkModeOutlined,
  LightModeOutlined,
  VisibilityOffOutlined,
  VisibilityOutlined
} from '@mui/icons-material';
import { useDispatch, useSelector } from 'react-redux';
import { loginRequest } from '../../redux/slices/login/api';
import { Alert, AlertTitle, IconButton, Tooltip } from '@mui/material';
import { removeLoginError } from '../../redux/slices/login';
import { useNavigate } from 'react-router-dom';
import CustomizeButton from '../../components/CustomizeButton';
import { swithTheme } from '../../redux/slices/theme/themeSlice';

const Login = () => {
  const { error, isLoading, auth } = useSelector((state) => state.loginReducer);
  const { isLight } = useSelector((state) => state.themeReducer);
  const dispatch = useDispatch();
  const navigate = useNavigate();

  const schema = yup.object().shape({
    email: yup.string().required().email(),
    password: yup.string().required().min(6)
  });

  const {
    control,
    handleSubmit,
    formState: { isValid }
  } = useForm({
    resolver: yupResolver(schema),
    mode: 'all',
    defaultValues: {
      email: 'admin@gmail.com',
      password: '123456'
    }
  });

  const [passType, setPassType] = useState('password');

  const submit = (data) => {
    dispatch(loginRequest(data));
  };

  const onChangePassType = () => setPassType(() => (passType === 'password' ? 'text' : 'password'));

  const handleClose = () => dispatch(removeLoginError());

  useEffect(() => {
    if (auth) navigate('/home');
  }, [auth]);
  const handleChangeTheme = () => dispatch(swithTheme());

  return (
    <div className={`${style.auth} ${!isLight ? style.dark : ''}`}>
      <div className={style.head}>
        <Tooltip onClick={handleChangeTheme} title={isLight ? 'light theme' : 'dark theme'}>
          <IconButton>{isLight ? <LightModeOutlined /> : <DarkModeOutlined />}</IconButton>
        </Tooltip>
      </div>
      <div className={style.background}></div>
      <div className={style.formContainer}>
        <div className={style.box}>
          <h1 className={style.title}>Welcome to dashboard</h1>
          <p className={style.p}>Please sign-in to your account and start the adventure</p>
          <Alert sx={{ marginTop: '30px' }} variant="outlined" severity="info">
            <p>Email: admin@gmail.com</p>
            <p>Password: 123456</p>
          </Alert>
          <form className={style.form} onSubmit={handleSubmit(submit)}>
            <div className={style.control}>
              <Controller
                render={({ field: { onChange, value }, fieldState: { error } }) => (
                  <AuthInput
                    value={value}
                    placeholder="Email"
                    onChange={onChange}
                    error={error}
                    type="text"
                  />
                )}
                name="email"
                control={control}
              />
            </div>
            <div className={style.control}>
              <Controller
                render={({ field: { onChange, value }, fieldState: { error } }) => (
                  <AuthInput
                    value={value}
                    placeholder="Password"
                    onChange={onChange}
                    error={error}
                    type={passType}
                    endAdornment={
                      <span onClick={onChangePassType} className={style.icon}>
                        {passType === 'password' ? (
                          <VisibilityOutlined />
                        ) : (
                          <VisibilityOffOutlined />
                        )}
                      </span>
                    }
                  />
                )}
                name="password"
                control={control}
              />
            </div>
            {error && (
              <div className={style.alert}>
                <Alert onClose={handleClose} severity="error">
                  <AlertTitle>Error</AlertTitle>
                  {error}
                </Alert>
              </div>
            )}
            <CustomizeButton
              size="large"
              fullWidth
              color="primary"
              loading={isLoading}
              disabled={!isValid || isLoading}
              type="submit"
              variant="contained"
              title="Login"
            />
          </form>
        </div>
      </div>
    </div>
  );
};

export default Login;
