export const formatCurrency = (number) => {
  const splited = number.toString().split('');

  const dotIndex = splited.findIndex((el) => el === '.');

  const firstHalf = dotIndex !== -1 ? splited.slice(0, dotIndex) : splited;

  const lastHalf = dotIndex !== -1 ? splited.slice(dotIndex) : [];

  for (let i = firstHalf.length - 3; i > 0; i -= 3) {
    firstHalf.splice(i, 0, ',');
  }

  const arr = [...firstHalf, ...lastHalf];

  const result = arr.length > 0 ? arr.reduce((a, b) => (a += b)) : 0;

  return result;
};
