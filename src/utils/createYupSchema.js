import * as yup from 'yup';

export function createYupSchema(schema, config) {
  const { id, validationType, validations = [] } = config;
  if (!yup[validationType]) {
    return schema;
  }
  let validator;
  if (validationType === 'number') {
    validator = yup[validationType]()
      .nullable(true)
      .transform((_, val) => (val ? Number(val) : null));
    //   .transform((_, val) => (val === Number(val) ? val : null));
  } else {
    validator = yup[validationType]();
  }
  validations.forEach((validation) => {
    const { params, type } = validation;
    if (!validator[type]) {
      return;
    }
    // console.log(type, params);
    validator = validator[type](...params);
  });
  schema[id] = validator;
  return schema;
}
