import React from 'react';
import { BrowserRouter, Navigate, Route, Routes } from 'react-router-dom';
import Dashboard from './components/Dashboard';
import Home from './pages/Home';
import Login from './pages/Login';
import Users from './pages/Users';
import Category from './pages/Category';
import ThemeContainer from './components/ThemeContainer';
import Products from './pages/Products';
import AddProductContainer from './pages/AddProductContainer';
import SelectProductType from './components/SelectProductType';
import AddProduct from './components/AddProduct';
import SingleProduct from './pages/SingleProduct';
import Orders from './pages/Orders';
import SingleOrder from './pages/SingleOrder';
import NotFound from './pages/NotFound';

const App = () => {
  return (
    <ThemeContainer>
      <BrowserRouter>
        <Routes>
          <Route path="login" element={<Login />}></Route>
          <Route path="/" element={<Dashboard />}>
            <Route path="/" element={<Navigate replace to="/home" />} />
            <Route path="home" element={<Home />} />
            <Route path="category" element={<Category />} />
            <Route path="users" element={<Users />} />
            <Route path="orders" element={<Orders />} />
            <Route path="orders/:id" element={<SingleOrder />} />

            <Route path="products" element={<Products />} />
            <Route path="products/:id" element={<SingleProduct />} />
            <Route path="addProduct" element={<AddProductContainer />}>
              <Route
                path="/addProduct"
                element={<Navigate replace to="/addProduct/selectType" />}
              />
              <Route path="selectType" element={<SelectProductType />} />
              <Route path="add" element={<AddProduct />} />
            </Route>
          </Route>
          <Route path="*" element={<NotFound />} />
        </Routes>
      </BrowserRouter>
    </ThemeContainer>
  );
};

export default App;
